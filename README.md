# README - Investing Tools #

### What is this repository for? ###

* Software for collecting and analysing investing data
* Version: 1.0.0

### How to setup, run and push changes to the software ###

Windows:

* Download Git from https://git-scm.com/downloads
* In Bitbucket click clone and copy the url
* Open Git Bash and type cd <insert the directory you want to clone the project into>
* Paste the URL into the command line and hit enter
* In a file explorer go to that directory and confirm the project was cloned to it
* Download the latest version of Python 3
* Download the Flask python library
* Run the investingTools.py script
* Go to http://128.199.224.150/ in a browser
* Watch a Git tutorial to learn how to manage changes to the software and push them to the Bitbucket remote repository

### Contribution guidelines ###

* Code review and submit suggestions or bug to Tim Thacker or Rob Pitrans

### Who do I talk to? ###

* Tim Thacker: timthacker97@gmail.com
* Rob Pitrans: pitransrobert@gmai.com

### Frameworks and Libraries ###

Libraries:

* Custom Internet Sweep Library
* Google Finance API Library https://pypi.python.org/pypi/googlefinance
* Yahoo Finance Library API Library

Frameworks:

* Flask

At the moment the front end will be an html5 website where users can see all the possible data about the ASX (Expand to other markets later) and have tools to analyse it in different ways (Trading, Long Term Value Investing...)

The back end of the website will be built with the Flask website framework (lightweight compared to Django) and a MySQL database. As well as our custom library of functions to sweep the internet and things.

### WebScraper Functions ###

getHistoricalCompanyData

updateHistoricalCompanyData

getHistoricalIndexData

updateHistoricalIndexData

getListedCompanies

updateListedCompanies

### TO DO ###

Templates (HTML):

* header.html
* footer.html
* index.html (Links all the different investing tools and features)
* company_research.html (Displays all the data on a company given in the URL or a list of the companies if no company code is given)
* profile.html (profile of users)
* login.html (Login page)
* forum.html (Main forum page)
* contact.html (Page where users can contact us and make suggestions or report bugs)
* about.html (Page where users and read about the founding of the website and things)

Database Functions ():

* sweepFunctions
    * updateListingsASX()
    * updateLiveDatabaseASX() => Sweeps Google fincance for live market data 
    * updateHistoryDatabaseASX() => sweeps for detailed history data

Sweep Functions (Functions that sweep the internet for stock data and put it into a database):

* updateCodes => updates the CSV file with all ("Company Name", Company Code, "Indusrty")
* readCodes => Read codes from csv file and return array
* getShareData => Sweep the internet and update the database

### Data Sources ###

* Google Finance API => Live Data
* <Insert source with the best historical data>

### Features (Think of this as the website navigation menu => Sub points can be in drop down) ###

Add these features starting from the top of the list

* Home page => links all the investing tools
* Company Research (enter a company and displays all the live and historical data of that company)
    * ASX
    * NASDAQ
    * etc.
* Investors Community
    * Forum
    * User Profiles
* Exchange rates markets
    * Traditional currency (USD, AUD)
    * Cryptic currency (bitcoin, e coin)
* Bank interest rates offered on term deposits and government bonds etc.
* Commodities
    * Gold
    * Oil
    * Silver
    * etc.
* Trading (Users and use their investing tools account to use our trading algorithm)
    * Futures
    * Options
    * Derivatives
* Forum, an open discussion board for users

##Market Specific Dashboard Layout##

* Basically my idea for the layout pretty much has 2 ads and 5 information boxes the information boxes are
    * Graph showing the movements of the day and has live movements as the day progresses, we could also include a drop down that can let you change the period of time that you are looking at as well as the specific market related index so the automatic is all ordinaries for one day but if users want to view the ASX 200 graph for a month that option is just in 2 separate drop down menus
    * Price percentage gains just shows the largest movements from day to day in the ASX, I think it would be more useful if the weight this through a simple algorithm like I was saying before that looks at a companies market cap and the movement and determines how important it is. Users should also be able to change between ASX 200 and various other indices
    * Price percentage losses, pretty much the same as above but only looks at losses.
    * Volume movements; shows the highest volume of shares traded for the day, although this is heavily weighted towards high market capped companies with low priced shares like telstra and the like. This is the only one that I'm unsure about.
    * Industry Select; lets you pick an industry and looks at the largest companies in that sector, so pretty much just picks out company industry and looks at market cap. Users can obviously select between industries and we keep the options to about 10-15 because people don't want to shift through 30 different industries to find what they are looking for. This section should take up all of the horizontal space for its line

* We then have 2 adds, the one up the top is slightly smaller than a normal box to give space to the graph then the second one down the page is in the third row and is a normal sized box

* We also put Shares > ASX at the top of the page so users know what page they're on.

* Have a look at the word doc I sent you for the layout, because bit bucket won't accept my picture drawing or my word doc

##Site Technicals##

* site permissions
    * admin
    * moderator
    * premium
    * user
    * banned

* Down the track we can add a map when you go all exchange, all foreign exchange and you can select different regions and then select the stock exchange that you're interested in.

## Research ###

### Services/Account Based Websites ###

#### Robo advisors ####

Wealthfront:

* You input your portfolio then it gives you an analysis on how risky it is, how much potential there is for improvement and suggests alternatives that you could invest in
Rebalance IRA
* Effectively a managed fund that uses compound interest on your funds to save you fees, rather than charging you a ‘simple interest’ fee they charge a compounding one so you save money and can grow your investment.

FutureAdvisor

* You can set goals that you can achieve once it analyses the riskiness of your goal and your investment

WiseBanyan

* A completely free financial advisor that makes its money through services that take individual factors as part of a set.

Sigfig

* Simple robo advice that also looks at how industries that you are invested in are performing at a given point in time which you can use, no fees are charged for accounts less than 10,000$

Betterment

* Encouraging investing site of sorts, you input age and money and get set goals to achieve, it recommends investments that are suited to you and the risk you want to undertake

FinMason

* You input your investments and it will make a risk assessment on them by measuring fluctuations and the amount that you have proportionally invested into those specific assets

Wealthfront

* Built for lazy people, you put in however much money you want and answer a set of questions on the type and style of investing you want to do and the site will invest with its algorithm and tinker with it in the future to take advantage of share movements.

#### Social Investing ####

iQuantifi

* You set goals for yourself which you then strive to achieve such as buy a car, they charge 9.95$ a month while it also gives you advice on possible investment decisions best suited to your situation. Overall a good motivator but still really expensive for what you get out of it.

Tip’d off

* Similar to openfolio but has inbuilt social media where you can discuss stocks and investment decision, again I think a poor app as it doesn’t use quantitative analysis well and has poor qualitative analysis

#### Other ####

Acorns

* Takes a small amount of your daily purchases for anything you buy and rounds it up to the nearest dollar, it takes this surplus which can then be invested, it charges $1 a month, this is more of an encouragement policy for people who want to invest but are spending all of their income

Kapitall

* Simulates investing, gives you 100,000 virtual dollars which you can invest in anything you want although it charges a 7.95$ commission, this is more for beginning investors who don’t know their way about but want to learn

Robinhood

* Offers $0 brokerage rates yet it makes its money by taking a percentage of idle cash held in accounts which will vary based on the amount of money that is in each account

Openfolio

* Lets you observe the investment decisions of people on social media, the idea is that you trust these people and they will have similar investment patterns to you, it only gives percentage allocations not dollar amounts to ensure privacy. I think it’s a pretty bad idea since people can be uninformed and this would be multiplied across people in your friends list

Motif Investing

* If you know something you want to invest in but don't know the specific companies associated with that industry you can use motif investing, so you put in 'plastic production' and it will give you a list of companies that deal with that field, it also has inbuilt trading services with a flat rate across all trades so you pay $4.95 even if you invest in 100 companies all at once as a bundle for the one trade.

#### Informational ####

Investopedia

* Gives you information on the definitions of economic products, investment and financial products

Yahoo! Finance

* Provides financial updates on shares as well as providing company reports and news for the company

Google finance

* Pretty much the same as Yahoo finance

MSN Money

* Provides raw financial information and graphs that give the quantitative side of companies globally

Zacks Investment Research

* Provides opinionated articles as well as financial information from a number of experts in the field of investing

Closed End Fund Centre

* Collates managed funds and determines which would be most suitable for you taking into account, returns, leverage and risk.

#### Advisors/Blogs ####

Motley Fool

* Experts who talk about what is going on in the markets it specialises in the American stock market but also has a site for Australia

The Street

* Experts such as Jim Cramer provide their opinion on the current state of the market

Wall Street Journal

* Looks at company specific news and opinionated articles but also holistic views on the market
* Also includes, as they are owned by WSJ (MarketWatch, Barron’s, Smart Money)

Investor Guide

* Gives indepth analysis on the market mostly focusing on the US market it also uses your investor style such as risky or blue chip to give you tailored articles to your specific investing strategy

Seeking Alpha

* Looks at reports and trending articles across many news sites to give you an idea of what the markets doing and the current perceptions of the stock market by media and individuals



Rob: My opinions/what we can do

* Overall there aren’t a whole lot of mainstream financial information sites that just go out and sweep other sites, most use Yahoo!, google or MSN finance for that kind of information. Some sites differ in this regard by offering opinionated articles by experts that gives investors more than just the numbers. Sites that I really like are things like investor guide, by tailoring its advice to your investment style it has a niche over its competitors, I don’t want an article for near retirees when I’m doing risky investing. I also think a good site is Zacks investment research that has a combination of quantitative and qualitative analysis on the stock market. Personally I think the social investing options are really dumb and leads to uninformed speculation among investors but it may be a good way to make money or sell data, depends on where you want the website and ethicacy to go. The robo advice section is really interesting, there are a lot of competitors some of which definitely sell your data to big data firms while others seem like they may really help with tinkering and adjusting how you have your portfolio set up. The goal base on some of these sites is interesting but kinda tacky in my opinion, if you are motivated for an investment you don’t need some bar graph to show how much more you need to buy a car. I do think these kind of sites would be hard to program as you need to take into account historical prices, price fluctuations, market capital and the overall risk of each investment from a pool of the ASX to get any good information from this and pass it onto consumers, although you could sell this for good prices.

* Overall I’m not 100% sure what you want to do with the information that we sweep but I think a good idea is something like seeking alpha but improve on the idea, so rather than just looking at who's talking about what you aggregate media to get an idea of public perceptions because ultimately this is what will dictate price changes. This is a whole new realm of big data and is pretty big in economics but those are just my thoughts, any ideas?

Tim:

* I'd like the end goal to be a finance/investing tools website. Centralising data in a massive database and also  having heaps of tools to analyse it
* We should come up with a list of features we want


Key Competitor sites

Tradeking

* You need to create an account but this will analyse certain shares that you put in, has a big amount of data on American companies (you can also only sign in if you live in America) with an inbuilt trading service that charges a flat $4.95. Also has calculators so you can find the profitability/riskiness/probability of movements by using historical data. Also deals with options trading with in built features that give technical analysis on set futures

The Australian Investor

* Has a lot of data on a large number of investment options, the site looks like its from 1999 though and is fairly cumbersome as you have to input your input as a first time user, likely to protect against ddos or some shit, regardless it still has some good data that I think we could use, it just has the worst UI ever, up to you if you want to troll through it for something useful.

Investing.com

* Has a tonne of data on so many companies globally, also includes key financial technical analysis of a manner of businesses, this should be a good place to sweep for info on what we need, also has some opinionated articles. Also has information on commodities and seems to use stochastics a lot for its advice on buying and selling.

User Interface Ideas

General UI ideas

* We should select one primary colour for the site as a whole, we should use this relatively sparingly as it attracts the eyes of users if we use an orange colour say that will highlight key important features for users like, the shares drop down menu or ‘create a user account’ the more we use this the less powerful it becomes, the less we use this the less users will be attracted to certain aspects of the site, the key is to find a balance between the two which I think should be in the main menu section and the user account specific fields

* We should use a white background on both sides of the site, this provides a bland area where the user won’t look too much and gives us space to put in banner ads and the like. We should them use a grey background for the middle section of the site, this subtly attracts the eye of the user to the important aspects of the site and to the overall data

* We want to keep drop down menus to a minimum, this hides the sites features and may make users think there are less options available for them than there actually are. Of course we will need a drop down or side swipe menu for the home page (more about this later) but the main thing is we use it minimally.

* The biggest thing about UI and website design is using space most effectively, when data and features are too spread out it makes the user disinterested as not everyone wants to scroll through troves of pages to get to their data. At the same time, you don’t want everything concentrated and dense because it makes users feel overwhelmed by the volume of information you are providing. So as a general rule I think for each page we should have as a maximum two windows, in other words for each page we have on the site you can take 2 screenshots of it at 100% zoom and that would have all the data we are presenting on that page. Obviously there’s more to it than that as you don’t want bucket loads of data for companies all compressed into tables but we’ll get to that with drawing design.

* For the drop down or side swiping menus that we use we should have them animated, say make it take 0.5 seconds to extend out, drop down menus that instantly pop out for the user can be annoying and detract from the overall site experience. So if we have a drop down menu for; my account, shares, about us etc. each one should have their own animation that slides out.

* We need to keep borders to a minimum, sites that use minimal borders are much more effective, so make it a maximum of 1 border for each tool or feature ![borders.PNG](https://bitbucket.org/repo/Mo6LMx/images/249063408-borders.PNG)

* We should always have a search bar at the top of every web page so users can quickly switch to different companies. I think it’s best if this uses suggestive measures, so if a user types in WO the first options should be things like Woolworths and also take into account typos (this is probably really difficult but it’s just a suggestion) so it might say ‘did you mean Wando?’ sites like investing.com does this really well but isn’t too great on the typo area.

* I’m not too sure how web size sizing works but we want it so you don’t have to use any side scroll options, if users have a 4K resolution screen does that change the size that data is presented at? Or pixilate it heavily? The important thing is that the site info is all conveyed in a screen size layout. As we progress we can also add a mobile site or an app which would then change the output methods of our data.

* Websites nowadays use social media sharing to excess In my opinion we should have a single link to our facebook page at the bottom of every page, none of the bullshit like “share this company on facebook” sure it may increase word of mouth about the site but it seems pesky as fuck to me, don’t know how you feel about it.

* Pretty much goes without saying but we should ue finance specific ads so advertisements to brokers that you can use (obviously we won’t get brokers paying us for ads as we start up that’s going to come with time but its important to keep in mind)

User Account Specific Ideas

* When users sign up they should have to input a fairly minimal amount of data, things like their email, username and name should be sufficient, I don’t think it’s necessary to know their address, it brings in issues of privacy and the users might not want to input anything due to time constraints. It’s possible in the future to analyse data about users so for instance we might find that a lot of users live in Sydney or a lot like a risky style of investing and then we can alter the sites design from there, but I think it’s more important that we just get users to sign up initially and ask for a minimal amount of data.

* Give users the option to personalise their account, encourage them to make their own portfolio, let them select the types of industries they are interested in and make this affect what they see on their homepage when they first login, same with their style of investing, if they are mostly looking at blue chip stocks it’s pretty meaningless to show them the biggest movers (like 12% movers) as this is generally penny stocks, this would be more valuable for risky investors. Maybe in the future we can allow users to customise the actual appearance of their site, change the colours etc. I get that this will be difficult but it may make users feel as if they have something invested in the site and may improve user retention and traffic. It’s important that we only encourage users to do this if you force them to make a portfolio when they first sign up they may be overwhelmed, just push them forward with the decision to do this. I get that Amazon has entire teams working on improving their algorithms for similar interest (products rather than shares) but we can improve this with time, say as we get a bigger and bigger user base we can see what other people are adding to their portfolio and use this as a basis for the algorithm we produce.

* For compulsory fields when users sign up we should indicate which areas are compulsory with a simple asterix so they know what’s wrong like if a username is already in use or a password doesn’t meet requirements. We should also include information about these fields so “password must be at least 6 characters long and contain a number”

* When users are creating their portfolio don’t just ask them to type in the companies that they want, people won’t remember off the top of their head all of the 15 companies that they want to keep track of, instead give them the option to search by industry, so say we have a drop down menu that gives an industry list like; banks, technology, resources, retail etc. then if they select this they get a set of popular choices in these areas. This will improve user experience and make users feel more invested as more likely than not they will have more companies in their portfolio than they otherwise would if this function wasn’t in the site.

* When looking at your portfolio you should be able to see a collection of data so it might sum up the average percentage movements of all companies that you presented or the average market capital, we can also include the ASX 200 or the total ASX movements to give the user an idea of how their portfolio is performing in comparison to the market as a whole, if you want to make this more advanced you can also allow users to select a handful of companies in their portfolio (kind of like holding control and selecting multiple) and getting the averages to change based off this.

* When a user is going through their portfolio give them the option to click on the company, this may then open up other data other than the price so give info on the market cap, dividend yield, 1 month movements etc. then have the further option of going to the webpage of the company itself and a direct link to this.

* I said before that we should have a short sign up phase but if this reaches 3 or more pages we should have things like ‘create my portfolio’ rather than just ‘next step’ this gives user an expectation of the next step and keep them more in touch with the sign up process itself.

* I think I’m talking a lot about portfolio but that’s because I think it’s really important, when users add companies to their portfolio a confirmation should pop up, something like “Telstra successfully added to technology portfolio” rather than just a blank screen we can also have “Telstra is already in technology portfolio” think of it as giving the user some transparency of what is going on in the background of the site rather than just shutting them off without any additional information.

Investing Specific Ideas

* When we present investing data we need to use headings and colours effectively so say we have the raw data for companies we should put a heading for this and include some colour, then use an absence of colour for the data itself, but then say when we get to the overview of the company that tells you a bit about it you have a new heading and colour that attracts attention to that area

* When we present table data we want to keep borders to a minimum and space it out (both discussed earlier) for a good example of this ![google.PNG](https://bitbucket.org/repo/Mo6LMx/images/3520211766-google.PNG), google finance does this really badly as they compress all of their data in preference for the graphed outcomes of the company whereas investing.com does this really well it doesn’t prioritise either one and spaces each one accordingly ![investing.PNG](https://bitbucket.org/repo/Mo6LMx/images/3760148738-investing.PNG)

* We should have different tabs for the more specific kinds of data or the general overview of the company so that you don’t overdo it for the casual user who just wants to look at the general price of the company, then the more sophisticated user might want to look at the specific dates at which dividends were given out and there should be a separate tab that redirects you to a separate webpage that has this specific kind of data. This will overall increase the space we have for users and enable more data to be presented to the user. So long as we colour these links in our primary colour this should be perfect.

* I know how you were talking about just using a java script for the graphs that we end up using but when we want the more technical types of graphs like stochastics and volume sales this may start to get more difficult. I think it’s important that we include this type of data because it gives more sophisticated data to the user an we can present it as a seperate graph just under it like google finance does ![GRAPHS.PNG](https://bitbucket.org/repo/Mo6LMx/images/772914589-GRAPHS.PNG) then users can also compare this to another technical graph so google lets you compare three different graphs all up. I’m not sure how this would be possible but I do know stochastics and the like is just an algorithm that uses historical prices and bases it off this but I have no idea how you would be able to input this on the backend of the site.

* Sites like google finance include news really well into their site, probably helps that they have an entire search engine to base this off, whats more they include news points on their grah

##Algorithms##
###Oscillating Indicators###
Whenever there is a formula with close this can be replaced with the current price for when it must be represented on a day to day graph
##Fast Stochastic Oscillator##
* Stochastics compares the closing price of a security by the range of prices over a certain time either adjusting the time period or by taking the moving average result
* The basic formula is
   * %K = 100(C – L14)/(H14 – L14)
      * C is the most recent closing price
      * L14 is the low point of the last 14 trading sessions
      * H14 is the highest point of the last 14 trading sessions.
      * %K is the current market rate for the currency pair.
   * %D is the 3 day SMA of %K
      * %D = (%K1 + %K2 + %K3) / 3
* The period of both the %K (generally 14) can be changed and the slow moving average of %D can be changed (generally 3) to give a faster or slower indicator
*Therefore, transition signals are created when the %K crosses the 3 period moving average (%D). This enables investors to look at the speed or the momentum of price.
* This then gives rise to the idea of overbought and oversold stocks, due to its percentage value it has a range of 0-100 where 80 is generally the overbought level and 20 is the oversold level
##Slow Stochastic Oscillator##
* The formula is
   * %K(s) = (%K1(f) + %K2(f) + %K3(f)) / 3
      * With the %K(f) representing the result of the fast indicator it effectively takes a slow moving average
   * %D = (%K(s)1 + %K(s)2 + %K(s)3) / 3
      * This then takes a slow moving average of the already slow moving average
* This results in a lagging type of indicator that doesn’t stick to prices as heavily and fluctuates less thus is used more by long term investors
##Chande Momentum Oscillator##
* The formula is
   * CMO = 100*((Su-Sd)/(Su+Sd))
      * Su is the sum of the difference between the current close and the previous day’s close
      * Sd is the sum of the absolute value between the current close and the previous day’s close
      * When the previous day’s close equals the next days close it is ignored
* Calculates the difference between the sum of recent gains and the sum of recent losses then divides the sum of all price movement over the period. It is bound within a range of 100 to -100
* Above 50 and below -50 are the ranges of overbought and oversold respectively
* As this only looks at the close of each day it is more of a long term trend as it doesn’t look between day to day
##Williams %R##
* The formula is
    * %R = (Highest high – closing price) / (Highest high – Lowest Low) * -100
* Is used to determine entry and exit points for the market comparing the close of the sock to the low and high range over a certain period of time (generally 14 days) which is the opposite of the stochastic oscillator
* Basically compares the current price and its relationship to the high and low over a set period of time
* It can give an indication of market movements as well as show overbought and oversold levels.
* The oscillator moves between 0 and -100, when they are 0 to -20 and -80 to -100 they are overbought and oversold respectively
##Oscillator- Moving Average (OsMA)##
* The formula is
   * OsMA = MACD – SMA(MACD, N)
      * MACD is the moving average convergence diversion
      * SMA is the simple moving average
      * N is the number of periods
      * SMA(MACD, N) is simply a signal line
* Shows the difference between the short term moving average and the long term moving average
* The indicator takes the difference between the MACD and the signal line and plots it on a graph, this is what the OsMA is
* A 12 period and a 26 period are the most common
* When the oscialltor is negative and positive it represents oversold and overbought respectively with a centre line is drawn at 0
##StochRSI##
* StochRSI = (RSI – Lowest RSI) / (Highest RSI – Lowest RSI)
   * RSI is the relative strength index
* 14 days is the typical time period of low and high RSI values
* An asset is oversold below 0.2 and overbought above 0.8 with a centre line drawn at 0.5
* The indicator fluctuates quickly which can be compensated by having a longer time period
##Exponential Moving Average (EMA)##
* The formula is
   * EMA = (Price (today) * K) + (EMA(yesterday) * (1-K))
      * K = 2 / (N+1)
         * N is the number of periods
         * If more periods are used, the weighting on latest prices will be reduced
         * As EMA is in the actual formula you should have a starting point of the closing price for the first data entry
* Is similar to a simple moving average although more weight is given to the latest data meaning that it will react faster and have higher volatility due to price changes
* There is minimal lag in these prices and will thus stay relatively close to respective prices and is thus between used as a short term indicator
* There is a typical time period of 10 days although it can be adjusted as seen fit
##Klinger Volume Oscillator (KVO)##
* The Formula is
   * KVO = EMA(34)(SV) – EMA(55)(SV)
      * EMA is the exponential moving average
      * To calculate SV you need to find the ‘typical price’ 
         * TP = (highest days’ price + lowest days’ price + closing days price) / 3 
      * Then the volume force is
         *  SV = (TP (today) – TP (yesterday)) * Volume today
      * 34 and 55 are the most optimal time periods that balance the oscillator as a medium term indicator although can be changed if desired
      * A trigger line of EMA(13)(KVO) can also be used
* The indicator is intended to have the sensitivity to indicate short term movements as well as the stability to show long term trends.
* Periods of 34 and 35 are typically used
* There is an indication to investors when the KVO crosses the trigger line.
##Simple Moving Average (SMA)##
* The formula is
   * SMA = (Price 1 + Price 2 … Price N)/N
      * All prices are closing prices
      * N is the number of periods used
* This gives an indication of trends and is a lagging indicator meaning it is more useful as a long term measure
* The longer the time frame the smoother the gradient will be
* This lets investors know if the stock is in an uptrend or a down trend
* There is a bearish trend when the 50 day SMA  drops below the 100 day SMA and a bullish trend if the 50 day SMA moves above the 100 day SMA these are called the death cross and the golden cross respectively
##Relative Strength Index (RSI)##
* The formula is
   * RSI = 100 – 100 / (1 + RS)
      * RS = Average gain / Average loss
        * All of these are expressed as positive numbers
      * First average Gain = sum of all gains over 14 periods / 14
      * First Average Loss = sum of all losses over 14 periods / 14
      * Following Average Gains = ((previous average gain) * 13 + current gain) / 14
      * Following Average Losses = ((previous average loss) * 13 + current loss) / 14
* RSI oscillates between zero and 100 
* Overbought is above 70 and oversold is below 30, however some traders instead opt for 80 -20 respectively depending on the volume of trading they are aiming for
* The typical period is 14 days although this can be adjusted with anything less than this being more volatile and anything more being less volatile
* Changes are seen as divergences with upswings signalling a bullish market and downswings showing a bearish market.
##Commodity Channel Index (CCI)##
* The formula is
    * CCI = (TP – SMA(TP)) / 0.015 * mean deviation
      * TP is the typical price TP = (high price + low price + closing price) / 3
      * SMA is the simple moving average generally a period of 20 days
      * 0.015 is a constant
      * Mean deviation
         * 1/n ∑ (i=1) | TP(n) – SMA(20) |
          * Where n generally equal 20
* This constant mean is used to ensure that 70 to 80 percent of values will be between 100 and -100
* A CCI with a shorter period will increase volatility and stick to the real time prices more with the opposite being true for a longer time period
* If the indicator is high it shows strength and that the price is above average while if it is negative it represents a downward trend, as it is a leading indicator it can demonstrate bullish and bearish trends
* Overbought and oversold levels will vary depending on the asset selected with things such as securities fluctuating more consequently in general overbought and oversold levels are roughly +-100 whereas securities are roughly +-200
##Rate of Change (ROC)##
* Also known as the momentum oscillator
* The formula is 
   * ROC = ((Closing Price – Closing price n periods ago) / Closing price n periods ago) * 100
* Is simply a percentage change from one price point to the next
* The long term trend is positive if both the 250 day ROC and 125 day ROC are positive
* Is more suited to stocks that fluctuate frequently
* 21 days is a typical period to use for ROC
##Moving Average Convergence/Divergence (MACD)##
* The formula is
   * MACD line = EMA(12) – EMA(26)
      * Where there is a period of 12 and 26 days respectively for exponential moving average
   * Signal line = 9 day EMA of the MACD line
   * A 9 day EMA of the MACD line itself is taken
   * MACD histogram = MACD – Signal line
* The values of 12, 26 and 9 are typically used although it can be changed depending on preferences
* A bullish signal occurs when the MACD line crosses above the signal line with a bearish signal when it crosses below, the strength of the movement will impact upon its level as well as its length of time
* There are no positive or negative extremes, instead investors should look at historical levels and use this as an indicator
* MACD isn’t intended to be used as a buy and sell indicator but rather represent short term upward and downward trends
##On Balance Volume (OBV)##
* The formula is
   * If the closing price is above the previous close, then 
      * OBV = Previous OBV + Current volume
   * If the closing price is below the prior close, then
      * OBV = Previous OBV – current volume 
   * If the closing price is equal to the prior close, then
      * Current OBV = Previous OBV
* A positive volume can represent higher demand and possibly foreshadow higher prices, a negative volume may foreshadow decreased prices due to less buyers
* The absolute value given for OBV isn’t important but rather the OBV line which gives a trend of the OBV as well as give an indication of potential support or resistance levels to movements in price
* A bullish divergence when OBV moves lower or forms a lower low as prices move higher and may represent a price reversal
* This is not a standalone indicator and is only useful when combined with other price indicators
##Volume Moving Average (VMA)##
* The formula is
   * VMA = sum of n volume bars / n
* The value of n can be changed to be more of a leading indicator by making it smaller or more of a lagging indicator if it is increased
* This is a fairly simplistic although dependant on trading styles can be beneficial, short term traders usually use a bar value of 1 minute over a period of a day meaning n is generally 30 or 40 whilst medium term traders might use bar of 1 day over a period of 6 months meaning n will equal 8
* This should generally fluctuate fairly highly and is often used by investors to predetermine if a share is likely to increase or decrease in price due to volume demand and supply although VWAP is more accurate at portraying this
##Volume Weighted Average Price (VWAP)##
* The Formula has multiple steps
   * Multiply Typical price by the periods volume
      * ((High Price + Low Price + Closing Price) / 3) * Volume
   * Add all of these values progressively to form a cumulative total
   * Add a running volume as this is going called a cumulative volume
   * VWAP = Cumulative (Volume * Typical price) / Cumulative volume
      * The cumulative will depend on how far you want the data to go back, if its for the day it will slowly build up as the day progresses although you can make it so it only builds up for the previous 1 hour of trading, making it more of a leading indicator
* It is generally a lagging indicator as it takes the previous average time period although can be adjusted to make it more of a leading indicator
* The time period can also vary with accumulations every minute for instance meaning that VWAP is calculated every minute but can also be adjusted
* This is a good measure for large corporations or large buy or sell orders demonstrating liquidity as these large transactions do not want to disrupt the market
* It can also be used as a measure of the efficiency of a trade, for instance if a trade was executed below the VWAP it would be considered a good fill with the opposite being true if it is made above the VWAP line
##Bollinger Bands (BOLL)##
* There are 3 bands
   * Middle band = SMA(20)
   * Upper band = SMA(20) + (20 day standard deviation of price *2)
      * Standard deviation is where each price over the 20 periods is subtracted by the mean and squared with all of them being added together, this is then divided all by n and square rooted
   * Lower band = SMA(20) - (20 day standard deviation of price *2)
* This is usually set at 20 periods although can be changed, similarly the standard deviations are set to 2 although can be altered although this multiplier should only be increased slightly for instance with an increase to 50 periods the multiplier should change to 2.1
* Triggers are given when the lower or upper band makes contact with the price (easier with an open-high-low-close chart), if it makes contact with the lower band it is generally a indication that the stock is undervalued historically or visa versa with the upper band.
* Although movements on the upper band represent strength while movements on the lower band show weakness, weather to buy or sell from this can be unclear.
* The bands should contain 89% of all the prices thus movements outside of this are important
* Investors often look instead for a convergence of these bands to look for patterns related to price movements.
##KDJ Indicator (KDJ)##
* The indicator has 3 bands
   * %K = 100* (close – lowest low of last n periods) / (highest high of last n periods – lowest low of last n periods)
   * %D = (%K1 + %K2 + %K3)/3
   * %J = (%D * 3) – (%K * 2)
* This is simply an extension of a fast stochastic indicator although fluctuations between the %K and %D bands can be observed with %J This will amplify the fluctuations of each of the bands and can go above 100 and below 0 thus it can put a larger emphasis on periods of overbought and oversold shares
* As this the fastest band of the 3 it is more useful for short term traders although could be used by longer term investors if they extend the periods
##Accumulated Distribution Line (ADL)##
* The formula has 3 steps
   * Money flow multiplier = ((close – low) – (high – close)) / (high – low)
      * The period is adjustable to determine which prices are used
   * Money flow volume = money flow multiplier * total volume over the period
   * ADL = previous ADL + current periods money flow volume
      * For the initial value the previous ADL is just 0
* This will fluctuate within a band of +1 or -1
* A high positive multiplier with a high volume indicates strong buying pressure, while a low volume with a low negative number will indicate a strong selling pressure thus the indicator can be used to either confirm a trend or question the stability of certain movements
* It is best used when placed behind the OHLC and movements in price can correspond to movements in the ADL, if the ADL increases or overtakes price it gives an indication that prices are lower than demand and thus represents a good time to buy and visa versa although investors must wait for unforeseen changes in volume rather than simply purchasing when ADL increases.
* Peaks in the ADL can foreshadow movements in price long in advance due to the laws of supply and demand

##ADX##

##ATR##

##Ultimate oscillator##

##Bull/Bear power##
 
##Other Technicals##
##Fibonacci##
* The formula is
   * HP - LP * 0.382
   * HP - LP * 0.5
   * HP - LP * 0.618
      * Hp is high price over a certain period of time
      * LP is low price over a certain period of time
* Is most effective when it is used between a period of sustained up ward or downward trend
* The idea is that when it is at the bottom band it is oversold, when it is at the top band it is overbought thus it sends these signals to the market and depending on how far back you go and the upward or downward trend you look at

##Camarilla##

##Woodies##

##DeMarks##

##Data for Investorgator###
##Market Data##
* Price
* Volume
* Day range #
* Week range #
* Month range #
* Year range #
* 52 week high #
* 52 week low #
* Month high #
* Month low #
* yesterdays close #
* Open price #
* Average volume #
   * 3 month #
   * 10 day #
* Market Cap #
* P/E ratio 
   * Trailing
   * Leading
* Bid
* Ask
* Shares outstanding 
* Earnings Per Share (EPS) #
* Dividend
   * Dates (announced/payed)
   * Taxed franked
   * Yield 
* Institutional ownership
* Float value

##General Company Info##
* Overview
* Industry
* Employees
* Location
* Executives; name, age, how long they’ve been there, title

##Company Reports##
* Current assets
   * Cash
   * Short term investments
   * Receivables
   * Total Inventory
   * Prepaid expenses
   * Other current assets
* Total assets
   * Property/plant/equipment
   * Accumulated depreciation
   * Goodwill
   * intangibles
   * Long term investments
* Liabilities
   * Long term debt
   * Total debt
   * Deferred income tax
   * Minority interest
* Equity
   * Redeemable stock
   * Preferred stock
   * Common stock
   * Additional paid in capital
   * Retained earnings
* Gross profit
* Operating expenses
   * Selling expense
   * Research and development
   * Depreciation
   * Interest expense
   * Unusual expense
   * Other operating expense
* Operating income
* Net income
* Profit before tax
* Profit after tax
* Revenue
* Next Earnings date
* Cash from operating activities
   * Depreciation
   * Amortization
   * Deferred taxes
   * Non-cash items
   * Cash receipts
   * Cash payments
   * Cash taxes paid
   * Cash interest paid
   * Changes in working capital
* Cash from investing activities
   * Capital expenditure
   * Other investment flows
* Cash from financing activities
   * Financing cash flow items
   * Dividends paid
* Foreign exchange effects
* Net change in cash
* Earnings before interest, tax, depreciation and amortization (EBITDA)
* Net income available to common shareholders
* Diluted EPS

##Forecasts by Company##
* Revenue
* EPS

##Accounting Ratios
* PEG ratio
   * 1 year
   * 5 years
* Profit margin
* Return on assets
* Return on equity
* Revenue per share
* Total cash per share
* Debt to equity
* Current ratio
* Debt to equity
* Book value per share (assets-liabilities)
* Quick ratio
* Interest coverage
* Leverage ratio
* Inventory turnover
* 







[Learn To Format README Document](https://bitbucket.org/tutorials/markdowndemo)