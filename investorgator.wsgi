#!/usr/bin/python
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/investorgator/")

from investorgator import app as application
application.secret_key = 'dskwxcw94lcmvjght0a23n45msdnc1ml'
