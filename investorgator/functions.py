from investorgator import app
from datetime import datetime

def getRelativeTimeString(time):
    now = datetime.now()
    
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
        
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "Just Now"
        if second_diff < 60:
            return str(int(second_diff)) + " Seconds Ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(int(second_diff / 60)) + " Minutes Ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(int(second_diff / 3600)) + " Hours Ago"
        
    if day_diff == 1:
        return "1 Day Ago"
    if day_diff < 7:
        return str(int(day_diff)) + " Days Ago"
    if int(day_diff / 7) == 1:
        return "1 Week Ago"
    if day_diff < 31:
        return str(int(day_diff / 7)) + " Weeks Ago"
    if int(day_diff / 30) == 1:
        return "1 Month Ago"
    if day_diff < 365:
        return str(int(day_diff / 30)) + " Months Ago"

    if int(day_diff / 365) == 1:
        return "1 Year Ago"
    
    return str(int(day_diff / 365)) + " Years Ago"