from flask import Flask, render_template, request

app = Flask(__name__)

import investorgator.stock_exchanges
import investorgator.investing_simulator
import investorgator.users
import investorgator.contact
import investorgator.forum
import investorgator.about
import investorgator.news

@app.errorhandler(404)
@app.errorhandler(500)
@app.errorhandler(401)
@app.errorhandler(400)
@app.errorhandler(403)
def page_not_found(e):
    return render_template('error.html', error = e)

@app.route('/')
def index():
    return render_template("index.html",
                            previousURL = request.referrer
    )