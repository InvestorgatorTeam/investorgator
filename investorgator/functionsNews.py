from investorgator import app
from investorgator.connection import connection
from investorgator.functions import *
from flask import session
from datetime import datetime
import os

def getArticles():
    c, conn = connection()
    
    c.execute("SELECT * FROM newsArticles WHERE deleted = FALSE AND draft = FALSE ORDER BY datetime DESC;")
    newsArticles = c.fetchall()
    
    newsArticleData = []
    
    for newsArticle in newsArticles:
        newsArticleRow = []
        newsArticleID  = newsArticle[0]
        userID         = newsArticle[1]
        title          = newsArticle[2]
        description    = newsArticle[3]
        dateTimeString = getRelativeTimeString(newsArticle[4])
        
        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]
        
        newsArticleRow.append(newsArticleID)
        newsArticleRow.append(title)
        newsArticleRow.append(description)
        newsArticleRow.append(dateTimeString)
        newsArticleRow.append(username)
        
        newsArticleData.append(newsArticleRow)
    
    c.close()
    conn.close()
    
    return newsArticleData

def addArticle(form, files):
    c, conn = connection()

    articleTitle   = form['articleTitle']
    description    = form['description']
    contents       = str(form['articleContents']).encode('utf-8')
    postType       = form['postType']
    dateTimeString = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    
    allowedExtentions = set(['png', 'jpg', 'jpeg', 'gif'])
    errors = []
    
    doesTitleExist = c.execute("SELECT newsArticleID FROM newsArticles WHERE userID = %s AND title = %s", [session['userID'], articleTitle])
    
    if len(articleTitle) == 0:
        errors.append("Please add an article title")
    if 'thumbnail' not in files:
        errors.append("Please upload a thumbnail")
    else:
        thumbnail = files['thumbnail']
        if thumbnail.filename == '':
            #errors.append("Please upload a thumbnail")
            pass
        elif not thumbnail and not allowed_file(thumbnail.filename, allowedExtentions):
            errors.append("Please upload a thumbnail of type png, jpg, jpeg or gif")
    if len(description) == 0:
        errors.append("Please add a description")
    if len(contents) <= 25:
        errors.append("Please write the article contents")
    if doesTitleExist != 0:
        errors.append("An article with this title already exists")
    if postType != "post" and postType != "save":
        errors.append("Invalid form type")
    
    if len(errors) == 0:
        if postType == "post":
            c.execute("INSERT INTO newsArticles (userID, title, description, datetime) VALUES (%s, %s, %s, %s)", [session['userID'], articleTitle, description, dateTimeString])
        elif postType == "save":
            c.execute("INSERT INTO newsArticles (userID, title, description, datetime, draft) VALUES (%s, %s, %s, %s, TRUE)", [session['userID'], articleTitle, description, dateTimeString])
        
        conn.commit()
        
        filename = '/var/www/investorgator/investorgator/static/img/articleThumbnails/' + str(c.lastrowid) + '.png'
        thumbnail.save(filename)
        
        c.execute("SELECT newsArticleID FROM newsArticles WHERE userID = %s AND title = %s", [session['userID'], articleTitle])
        newsArticleID = c.fetchone()[0]
        
        contentsArray = []
        while contents:
            contentsArray.append(contents[:65535])
            contents = contents[65535:]
            
        sequence = 0
        
        for row in contentsArray:
            contents = row
            
            c.execute("INSERT INTO newsArticleContents (newsArticleID, contents, sequence) VALUES (%s, %s, %s)", [newsArticleID, contents, sequence])
            conn.commit()
            
            sequence += 1
        
    c.close()
    conn.close()
    
    return errors

def allowed_file(filename, allowedExtentions):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allowedExtentions
    
def editArticle(form, files, newsArticleID):
    c, conn = connection()

    articleTitle   = form['articleTitle']
    description    = form['description']
    contents       = form['articleContents']
    
    allowedExtentions = set(['png', 'jpg', 'jpeg', 'gif'])
    errors = []
    
    doesTitleExist = c.execute("SELECT title FROM newsArticles WHERE title = %s AND newsArticleID != %s", [articleTitle, newsArticleID])
    
    if doesTitleExist > 0:
        errors.append("An article with this title already exists")
    if len(articleTitle) == 0:
        errors.append("Please add an article title")
    if len(description) == 0:
        errors.append("Please add a description")
    if len(contents) <= 25:
        errors.append("Please write the article contents")
    if 'thumbnail' in files:
        thumbnail = files['thumbnail']
        if thumbnail.filename != '':
            if not thumbnail and not allowed_file(thumbnail.filename, allowedExtentions):
                errors.append("Please upload a thumbnail of type png, jpg, jpeg or gif")
            else:
                filename = '/var/www/investorgator/investorgator/static/img/articleThumbnails/' + str(newsArticleID) + '.png'
                thumbnail.save(filename)
    
    if len(errors) == 0:
        c.execute("UPDATE newsArticles SET title = %s, description = %s WHERE newsArticleID = %s", [articleTitle, description, newsArticleID])
        conn.commit()
        
        c.execute("DELETE FROM newsArticleContents WHERE newsArticleID = %s", [newsArticleID])
        conn.commit()
        
        contentsArray = []
        while contents:
            contentsArray.append(contents[:65535])
            contents = contents[65535:]
            
        sequence = 0
        
        for row in contentsArray:
            contents = row
            
            c.execute("INSERT INTO newsArticleContents (newsArticleID, contents, sequence) VALUES (%s, %s, %s)", [newsArticleID, contents, sequence])
            conn.commit()
            
            sequence += 1
    
    c.close()
    conn.close()
    
    return errors
    
def deleteArticle(form):
    newsArticleID = form['articleID']

    c, conn = connection()
        
    c.execute("UPDATE newsArticles SET deleted = TRUE WHERE newsArticleID = %s AND userID = %s", [newsArticleID, session['userID']])
    conn.commit()
    
    c.close()
    conn.close()
    
def getArticleData(newsArticleID, type):
    c, conn = connection()
    
    message    = None
    isDeleted  = False
    isDraft    = False
    
    if type == "post":
        if not session.get('logged_in'):
            doesArticleExist = c.execute("SELECT * FROM newsArticles WHERE newsArticleID = %s AND deleted = FALSE AND draft = FALSE", [newsArticleID])
            if doesArticleExist > 0:
                newsArticleData = c.fetchone()
                doesArticleExist = True
            else:
                doesArticleExist = False
        else:
            if session['permission'] == 1:
                doesArticleExist = c.execute("SELECT * FROM newsArticles WHERE newsArticleID = %s", [newsArticleID])
                if doesArticleExist > 0:
                    newsArticleData = c.fetchone()
                    deleted = newsArticleData[5]
                    draft   = newsArticleData[6]
                    doesArticleExist = True
                    if draft == True:
                        message = "You are viewing this in admin preview mode, this article is a draft"
                        isDraft = True
                    elif deleted == True:
                        message = "You are viewing this in admin preview mode, this article has been deleted"
                        isDeleted = True
                else:
                    doesArticleExist = False
            elif session['permission'] == 2:
                doesArticleExist = c.execute("SELECT * FROM newsArticles WHERE newsArticleID = %s", [newsArticleID])
                if doesArticleExist > 0:
                    newsArticleData = c.fetchone()
                    userID  = newsArticleData[1]
                    deleted = newsArticleData[5]
                    draft   = newsArticleData[6]
                    
                    if deleted == False and draft == False:
                        doesArticleExist = True
                    elif userID == session['userID']:
                        doesArticleExist = True
                        if draft == True:
                            message = "You are viewing this in preview mode, this article is a draft"
                            isDraft = True
                        elif deleted == True:
                            message = "You are viewing this in preview mode, this article has been deleted"
                            isDeleted = True
                    else:
                        doesArticleExist = False
                else:
                    doesArticleExist = False
            else:
                doesArticleExist = c.execute("SELECT * FROM newsArticles WHERE newsArticleID = %s AND deleted = FALSE AND draft = FALSE", [newsArticleID])
                if doesArticleExist > 0:
                    newsArticleData = c.fetchone()
                    doesArticleExist = True
                else:
                    doesArticleExist = False
    elif type == "edit":
        doesArticleExist = c.execute("SELECT * FROM newsArticles WHERE newsArticleID = %s", [newsArticleID])
        if doesArticleExist > 0:
            newsArticleData = c.fetchone()
            doesArticleExist = True
        else:
            doesArticleExist = False
    
    if doesArticleExist == True:
        newsArticleID  = newsArticleData[0]
        userID         = newsArticleData[1]
        title          = newsArticleData[2]
        description    = newsArticleData[3]
        dateTimeString = getRelativeTimeString(newsArticleData[4])
        
        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]
        
        c.execute("SELECT contents FROM newsArticleContents WHERE newsArticleID = %s", [newsArticleID])
        newsArticleContents = c.fetchall()
        
        contents = ""
        
        for row in newsArticleContents:
            contents = contents + row[0]
        
        newsArticleData = []
        
        newsArticleData.append(newsArticleID)
        newsArticleData.append(username)
        newsArticleData.append(title)
        newsArticleData.append(description)
        newsArticleData.append(dateTimeString)
        newsArticleData.append(contents)
        newsArticleData.append(message)
        newsArticleData.append(isDeleted)
        newsArticleData.append(isDraft)
        
        c.close()
        conn.close()
        
        return newsArticleData
    else:
        return None

def getComments(newsArticleID):
    c, conn = connection()

    c.execute("SELECT newsArticleCommentID, userID, comment, datetime FROM newsArticleComments WHERE newsArticleID = %s ORDER BY datetime DESC;", [newsArticleID])
    commentData = c.fetchall()

    comments = []

    for comment in commentData:
        newsArticleCommentID = comment[0]
        userID               = comment[1]
        contents             = comment[2]
        dateTimeString       = getRelativeTimeString(comment[3])

        commentRow = []

        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]

        commentRow.append(newsArticleCommentID)        
        commentRow.append(contents)
        commentRow.append(username)
        commentRow.append(dateTimeString)

        comments.append(commentRow)

    c.close()
    conn.close()

    return comments

def addComment(newsArticleID, userID, comment):
    errors = []
    
    c, conn = connection()
    
    doesNewsArticleExist = c.execute("SELECT newsArticleID FROM newsArticles WHERE newsArticleID = %s", [newsArticleID])

    if len(comment) > 140:
        errors.append("Comment too long")
    if doesNewsArticleExist == 0:
        errors.append("News article doesn't exist")
    if not errors:
        dateTimeString = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        c.execute("INSERT INTO newsArticleComments (newsArticleID, userID, comment, datetime) VALUES (%s, %s, %s, %s)", [newsArticleID, userID, comment, dateTimeString])
        conn.commit()

    c.close()
    conn.close()

    return errors

def getAuthorArticles():
    c, conn = connection()
    
    c.execute("SELECT * FROM newsArticles WHERE userID = %s AND deleted = FALSE AND draft = FALSE ORDER BY datetime DESC;", [session['userID']])
    drafts = c.fetchall()
    
    newsArticleData = []
    
    for newsArticle in drafts:
        newsArticleRow = []
        newsArticleID  = newsArticle[0]
        userID         = newsArticle[1]
        title          = newsArticle[2]
        description    = newsArticle[3]
        dateTimeString = getRelativeTimeString(newsArticle[4])
        
        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]
        
        newsArticleRow.append(newsArticleID)
        newsArticleRow.append(title)
        newsArticleRow.append(description)
        newsArticleRow.append(dateTimeString)
        newsArticleRow.append(username)
        
        newsArticleData.append(newsArticleRow)
    
    c.close()
    conn.close()
    
    return newsArticleData

def getOtherAuthorArticles():
    if session['permission'] == 1:
        c, conn = connection()
        
        c.execute("SELECT * FROM newsArticles WHERE userID != %s AND deleted = FALSE AND draft = FALSE ORDER BY datetime DESC;", [session['userID']])
        drafts = c.fetchall()
        
        newsArticleData = []
        
        for newsArticle in drafts:
            newsArticleRow = []
            newsArticleID  = newsArticle[0]
            userID         = newsArticle[1]
            title          = newsArticle[2]
            description    = newsArticle[3]
            dateTimeString = getRelativeTimeString(newsArticle[4])
            
            c.execute("SELECT username FROM users WHERE userID = %s", [userID])
            username = c.fetchone()[0]
            
            newsArticleRow.append(newsArticleID)
            newsArticleRow.append(title)
            newsArticleRow.append(description)
            newsArticleRow.append(dateTimeString)
            newsArticleRow.append(username)
            
            newsArticleData.append(newsArticleRow)
        
        c.close()
        conn.close()
        
        return newsArticleData
    else:
        return None
    
def getDrafts():
    c, conn = connection()
    
    c.execute("SELECT * FROM newsArticles WHERE userID = %s AND draft = TRUE", [session['userID']])
    drafts = c.fetchall()
    
    newsArticleData = []
    
    for newsArticle in drafts:
        newsArticleRow = []
        newsArticleID  = newsArticle[0]
        userID         = newsArticle[1]
        title          = newsArticle[2]
        description    = newsArticle[3]
        dateTimeString = getRelativeTimeString(newsArticle[4])
        
        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]
        
        newsArticleRow.append(newsArticleID)
        newsArticleRow.append(title)
        newsArticleRow.append(description)
        newsArticleRow.append(dateTimeString)
        newsArticleRow.append(username)
        
        newsArticleData.append(newsArticleRow)
    
    c.close()
    conn.close()
    
    return newsArticleData
    
def getDeletedArticles():
    c, conn = connection()
    
    c.execute("SELECT * FROM newsArticles WHERE userID = %s AND deleted = TRUE", [session['userID']])
    deletedArticles = c.fetchall()
    
    newsArticleData = []
    
    for newsArticle in deletedArticles:
        newsArticleRow = []
        newsArticleID  = newsArticle[0]
        userID         = newsArticle[1]
        title          = newsArticle[2]
        description    = newsArticle[3]
        dateTimeString = getRelativeTimeString(newsArticle[4])
        
        c.execute("SELECT username FROM users WHERE userID = %s", [userID])
        username = c.fetchone()[0]
        
        newsArticleRow.append(newsArticleID)
        newsArticleRow.append(title)
        newsArticleRow.append(description)
        newsArticleRow.append(dateTimeString)
        newsArticleRow.append(username)
        
        newsArticleData.append(newsArticleRow)
    
    c.close()
    conn.close()
    
    return newsArticleData
    
def undoDelete(form):
    articleID = form['articleID']
    
    c, conn = connection()
    
    c.execute("UPDATE newsArticles SET deleted = FALSE WHERE newsArticleID = %s", [articleID])
    conn.commit()
    
    c.close()
    conn.close()
    
def permanentlyDelete(form):
    articleID = form['articleID']
    
    c, conn = connection()
    
    c.execute("DELETE FROM newsArticles WHERE newsArticleID = %s", [articleID])
    conn.commit()
    
    c.close()
    conn.close()
    
def deleteDraft(form):
    articleID = form['articleID']
    
    c, conn = connection()
    
    c.execute("UPDATE newsArticles SET deleted = TRUE, draft = FALSE WHERE newsArticleID = %s", [articleID])
    conn.commit()
    
    c.close()
    conn.close()
    
def postDraft(form):
    articleID      = form['articleID']
    dateTimeString = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    
    c, conn = connection()
    
    c.execute("UPDATE newsArticles SET draft = FALSE, datetime = %s WHERE newsArticleID = %s", [dateTimeString, articleID])
    conn.commit()
    
    c.close()
    conn.close()
    
def sendToDrafts(form):
    articleID = form['articleID']
    
    c, conn = connection()
    
    c.execute("UPDATE newsArticles SET draft = TRUE, deleted = FALSE WHERE newsArticleID = %s", [articleID])
    conn.commit()
    
    c.close()
    conn.close()