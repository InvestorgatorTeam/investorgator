from investorgator import app
from flask import Flask, request, render_template, redirect, url_for
from investorgator.connection import connection
from MySQLdb import escape_string as thwart
import gc

@app.route('/forum')
def forum():
    c, conn = connection()
    c.execute("SELECT * FROM forumCategories")
    forumCategories = c.fetchall()
    
    forumCategoriesList = []
    
    for category in forumCategories:
        categoryInfo = []
        c.execute("SELECT subCategoryName FROM forumSubCategories WHERE forumCategoryID = (%s)", [thwart(category[0])])
        subCategoryNames = c.fetchall()
        
        for item in category:
            categoryInfo.append(item)
            
        for subCategory in subCategoryNames:
            categoryInfo.append(subCategory[0])
        
        forumCategoriesList.append(categoryInfo)
    
    c.close()
    conn.close()
    gc.collect()
    
    return render_template("community/forum.html", forumCategoriesList = forumCategoriesList)
    
@app.route('/forum/<subCategoryName>/', methods=['GET'])
def forum_posts(subCategoryName=False):
    #Display all the posts from a given subcategory
    c, conn = connection()
    
    c.execute("SELECT forumSubCategoryID FROM forumSubCategories WHERE subCategoryName = (%s)", [thwart(subCategoryName)])
    forumSubCategoryID = c.fetchone()[0]
    
    c.execute("SELECT forumPostID, userID, title FROM forumPosts WHERE forumSubCategoryID = (%s)", [thwart(forumSubCategoryID)])
    posts = c.fetchall()
    
    for post in posts:
        c.execute("SELECT username FROM users WHERE userID = (%s)", [thwart(post[0])])
        username = c.fetchone()[0]
    
    c.close()
    conn.close()
    gc.collect()
    return render_template("community/forum_posts.html", subCategoryName = subCategoryName, posts = posts)

@app.route('/forum/<subCategoryName>/<forumPostID>', methods=['GET'])
def forum_post(subCategoryName=False, forumPostID=False):
    c, conn = connection()
    
    c.execute("SELECT title FROM forumPosts WHERE forumPostID = (%s)", [thwart(forumPostID)])
    forumPostTitle = c.fetchone()[0]
    
    c.execute("SELECT content FROM forumPostContent WHERE forumPostID = (%s)", [thwart(forumPostID)])
    contents = c.fetchone()[0]
    
    c.close()
    conn.close()
    gc.collect()
    return render_template("community/forum_post.html", subCategoryName = subCategoryName, forumPostTitle = forumPostTitle, contents = contents)