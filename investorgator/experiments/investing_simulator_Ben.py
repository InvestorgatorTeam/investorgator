import bs4 as bs
import datetime as dt
import os
import pandas as pd
import pandas_datareader.data as web
import pickle
import requests
import csv
from pandas_datareader._utils import RemoteDataError


def get_data_from_yahoo():

    tickers = []

    if not os.path.exists('stock_dfs'):
        os.makedirs('stock_dfs')

    start = dt.datetime(2000, 1, 1)
    end = dt.datetime(2016, 12, 31)

    with open("ASXListedCompanies.csv") as tickersfile:
        reader = csv.reader(tickersfile, delimiter=',')
        count = 0
        for row in reader:
            if count >= 3:
                tickers.append(row[1])
            count += 1

    print (tickers)
            


    for ticker in tickers:
        print(ticker)
        if not os.path.exists('stock_dfs/{}.csv'.format(ticker)):
            try:
                df = web.DataReader("{}.AX".format(ticker), 'yahoo', start, end)
                df.to_csv('stock_dfs/{}.csv'.format(ticker))
            except RemoteDataError:
                print("Stock data for {} could not be pulled from yahoo".format(ticker))
        else:
            print("Already have {}".format(ticker))


#get_data_from_yahoo()

class Game:

    def __init__(self, start, players, startCash):
        self.date = start
        # A list of portfolio names to be initialised
        self.players = []
        for name in players:
            player = Portfolio(self, name, self.date, startCash)
            self.players.append(player)

        self.open = True
        self.numPlayers = len(self.players)
        self.whoseTurn = 0

    def next_day(self):
        self.date += dt.timedelta(days=1)
        for player in self.players:
            player.update(self)

    def do_action(self, portfolioCode, action):
        if self.open == False and action["code"] != "Pass":
            return "Market is closed, cannot {}".format(action["code"])
        if portfolioCode == self.whoseTurn:
            p = self.players[portfolioCode]
        else:
            return "It's not your turn, self destructing"

        s = Stock(self)

        #try:
        if action["code"] == "Buy":
            s = p.buy_stock(self, action["ticker"], action["quantity"])
            print("Bought {} of {} at {} per share, total value ${:f}".format(s.quantity, s.ticker, s.startPrice, s.value))

            return s

        if action["code"] == "Sell":
            s = p.sell_stocks(self, action["stock"], action["quantity"])
            print("Sold {} of {} at {} per share. Net gain/loss of ${:f}".format(s.quantity, s.ticker, s.price, s.value-s.startPrice*s.quantity))
        #except TypeError as e:
            #print("Cannot make action because couldn't find the data in the csv")


        if action["code"] == "Pass":
            # Rotate through the players
            self.whoseTurn = (self.whoseTurn + 1) % self.numPlayers
            # If the current player is now 0, then its a new day
            if self.whoseTurn == 0:
                self.next_day()


    def get_player(self):
        return self.whoseTurn

    def get_date(self):
        return self.date


    def get_price(self, ticker):
        date = self.get_date()
        with open("stock_dfs/{}.csv".format(ticker)) as data:
            reader = csv.reader(data, delimiter=",")
            for row in reader:
                #print(date.isoformat())
                if date.isoformat() in row:
                    #print("Price on {}: {}".format(date.isoformat(), row[1]))
                    return float(row[1])

            # Not finished yet

class Stock:

    def __init__ (self, g, ticker="___", quantity=0, date=dt.date(1,1,1), startPrice=None):
        self.ticker = ticker
        self.quantity = quantity
        self.date = date
        self.startPrice = 0
        self.price = 0
        self.value = 0

        if ticker != "___":
            self.value = g.get_price(self.ticker)*self.quantity

            if startPrice == None:
                self.startPrice = g.get_price(self.ticker)
                self.price = self.startPrice
            else:
                self.startPrice = startPrice
                self.price = g.get_price(self.ticker)

    def sell(self, g, quantityToSell):
        if self.quantity >= quantityToSell:
            self.quantity -= quantityToSell
            self.update(g)
            # Returns a new stock object that represents the stocks that
            # have just been sold / removed so that we can get their
            # value and such
            return Stock(g, self.ticker, quantityToSell, self.date, self.startPrice)

    def update(self, g):
        self.price = g.get_price(self.ticker)
        self.value = self.price*self.quantity

class Portfolio:

    def __init__ (self, g, name, start, cash):
        self.name = name
        self.date = start
        self.cash = cash
        self.portValue = 0
        self.stocks = []

    def buy_stock(self, g, ticker, quantity):
        purchased = Stock(g, ticker, quantity, g.get_date())
        self.stocks.append(purchased)
        self.cash -= purchased.value
        self.portValue += purchased.value
        return purchased

    def sell_stocks(self, g, stock, quantityToSell):
        if stock in self.stocks:
            if quantityToSell > stock.quantity:
                quantityToSell = stock.quantity
            soldStocks = stock.sell(g, quantityToSell)
            self.cash += soldStocks.value
            if stock.quantity == 0:
                self.stocks.remove(stock)
        return soldStocks

    def display(self):
        print(self.stocks)

    def update(self, g):
        # recalculate portValue
        self.portValue = 0
        for stock in self.stocks:
            stock.update(g)
            self.portValue += stock.value

g = Game(dt.date(2005, 1, 3), ["Ben", "Me"], 5000)

while True:

    bought = g.do_action(0, {"code": "Buy", "ticker": "WOW", "quantity": 10})

    print(g.players[0].name + " has ${:.2f}".format(g.players[0].cash) + " cash and ${} in stocks".format(g.players[0].portValue))

    g.next_day()
    g.do_action(0, {"code": "Sell", "stock": g.players[0].stocks[0], "quantity": 11})

    g.next_day()

    print(g.players[0].name + " has ${:.2f}".format(g.players[0].cash) + " cash and ${} in stocks".format(g.players[0].portValue))
    print(g.players[0].stocks)

#player = Portfolio(g, "Me", dt.date(2000, 1, 1), 5000)



