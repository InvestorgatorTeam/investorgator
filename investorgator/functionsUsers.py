from investorgator import app
from investorgator.connection import connection
from flask import session
from passlib.hash import sha256_crypt
import urllib.parse
import html

#Register Functions

def registerErrorCheck(form):
    errors = []
    
    username       = form['username']
    password       = form['password']
    passwordRepeat = form['passwordRepeat']
    email          = form['email']
    
    c, conn           = connection()
    doesUsernameExist = c.execute("SELECT * FROM users WHERE username = (%s)", [username])
    doesEmailExist    = c.execute("SELECT * FROM users WHERE email = (%s)", [email])
    
    if int(doesUsernameExist) > 0:
        errors.append("Sorry, the username " + username + " is already taken")
    elif len(username) < 6 or len(username) > 32:
        errors.append("Please enter a username between 6 and 32 characters")
    if len(password) < 6:
        errors.append("Please enter a password with 6 or more characters")
    elif password != passwordRepeat:
        errors.append("Passwords did not match")
    if username.lower() == password.lower():
        errors.append("Username and password cannot be the same")
    if int(doesEmailExist) > 0:
        errors.append("Sorry, the email address " + email + " has already been used")
    elif len(email) < 6 or len(email) > 255:
        errors.append("Email must be between 6 and 255 characters")
    
    c.close()
    conn.close()
        
    return errors
    
def registerUser(form):
    username       = form['username']
    password       = form['password']
    passwordRepeat = form['passwordRepeat']
    email          = form['email']
    
    c, conn  = connection()
    password = sha256_crypt.encrypt(str(password))
    
    c.execute("INSERT INTO users (username, password, email) VALUES (%s, %s, %s)", [username, password, email])
    conn.commit()
    
    c.execute("SELECT userID FROM users WHERE username = %s", [username])
    userID = c.fetchone()[0]
    
    app.secret_key = 'dskwxcw94lcmvjght0a23n45msdnc1ml'
    session['logged_in']  = True
    session['userID']     = userID
    session['username']   = username
    session['email']      = email
    session['permission'] = 3
    
    c.close()
    conn.close()

#Login Functions

def getLoginErrors(form):
    errors = []
    
    attemptedUsername = form['username']
    attemptedPassword = form['password']
    
    c, conn       = connection()
    doesUserExist = c.execute("SELECT * FROM users WHERE username = (%s)", [attemptedUsername])
    userInfo      = c.fetchone()

    c.close()
    conn.close()
    
    #Check if a username and password was entered
    if not (attemptedUsername and attemptedPassword):
        errors.append("Please enter and username and password")
    
    #Check if username exists
    elif int(doesUserExist) == 0:
        errors.append("Invalid username or password")
    
    #Check if passwords match
    else:
        userID   = userInfo[0]
        password = userInfo[2]
        password = userInfo[2]
        if not sha256_crypt.verify(attemptedPassword, password):
            errors.append("Invalid username or password")
    
    return errors, userInfo

def loginUser(userInfo):    
    userID     = userInfo[0]
    username   = userInfo[1]
    password   = userInfo[2]
    email      = userInfo[3]
    permission = userInfo[4]
    
    app.secret_key = 'dskwxcw94lcmvjght0a23n45msdnc1ml'
    
    session['logged_in']  = True
    session['userID']     = userID
    session['username']   = username
    session['email']      = email
    session['permission'] = permission

#Logout Functions

def logoutUser():
    session.pop('logged_in'  , None)
    session.pop('userID'     , None)
    session.pop('username'   , None)
    session.pop('email'      , None)
    session.pop('permission' , None)
    session.pop('previousURL', None)

#Portfolio Functions

def getPortfolioErrors(portfolioName):
    c, conn = connection()
    
    errors = []
    
    c.execute("SELECT name FROM portfolios WHERE userID = (%s)", [session['userID']])
    existingNames = c.fetchall()
    
    for name in existingNames:
        if name[0] == portfolioName:
            errors.append("Portfolio already exists")
            break
    
    if len(portfolioName) == 0 or len(portfolioName) > 45:
        errors.append("Please enter a name between 1 and 45 characters")
        
    c.close()
    conn.close()
    
    return errors

def addPortfolio(form):
    errors = []
    
    portfolioName = form['portfolioName']

    errors = getPortfolioErrors(portfolioName)
    
    if len(errors) == 0:
        c, conn = connection()
        doesEntryExist = c.execute("INSERT INTO portfolios (userID, name) VALUES (%s, %s)", [session['userID'], portfolioName])
        conn.commit()
        c.close()
        conn.close()

    return errors

def checkAddCompany(form):
    if form['portfolio'] == "":
        return True
    if form['units'] == "":
        return True
    if int(form['units']) < 1:
        return True
    
    return False

def addCompanyToNewPortfolio(form):
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    units           = int(form['units'])
    userID          = session['userID']
    
    c, conn = connection()
    
    c.execute("INSERT INTO portfolios (userID, name) VALUES (%s, %s)", [userID, portfolioName])
    conn.commit()
    
    c.execute("SELECT portfolioID FROM portfolios WHERE userID = (%s) AND name = (%s)", [userID, portfolioName])
    portfolioID = c.fetchone()[0]
    
    c.execute("INSERT INTO portfolioCompanies (`portfolioID`, `companyID`, `stockExchangeID`, `units`) VALUES (%s, %s, %s, %s)", [portfolioID, companyID, stockExchangeID, units])
    conn.commit()
    
    c.close()
    conn.close()

def addCompanyToPortfolio(form):
    portfolioID     = form['portfolio']
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    units           = int(form['units'])
    userID          = session['userID']
    
    c, conn = connection()
    doesEntryExist  = c.execute("SELECT units FROM portfolioCompanies WHERE portfolioID = (%s) AND companyID = (%s) AND stockExchangeID = (%s)", [portfolioID, companyID, stockExchangeID])
                    
    if doesEntryExist > 0:
        currentUnits = c.fetchone()[0]
        updatedUnits = currentUnits + units
        c.execute("UPDATE portfolioCompanies SET units = (%s) WHERE portfolioID = (%s) AND companyID = (%s) AND stockExchangeID = (%s)", [updatedUnits, portfolioID, companyID, stockExchangeID])
        conn.commit()
    else:
        c.execute("INSERT INTO portfolioCompanies (`portfolioID`, `companyID`, `stockExchangeID`, `units`) VALUES (%s, %s, %s, %s)", [portfolioID, companyID, stockExchangeID, units])
        conn.commit()
        
    c.close()
    conn.close()
    
def deletePortfolioCompany(form):
    c, conn = connection()
    
    portfolioID     = form['portfolioID']
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    
    c.execute("DELETE FROM portfolioCompanies WHERE portfolioID = %s AND stockExchangeID = %s AND companyID = %s", [portfolioID, stockExchangeID, companyID])
    conn.commit()
    
    c.close()
    conn.close()

def editCompanyUnits(form):
    c, conn = connection()
    
    portfolioID     = form['portfolioID']
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    units           = form['units']
    
    if units == 0:
        c.execute("DELETE FROM portfolioCompanies WHERE portfolioID = %s AND stockExchangeID = %s AND companyID = %s", [portfolioID, stockExchangeID, companyID])
    else:
        c.execute("UPDATE portfolioCompanies SET units = (%s) WHERE portfolioID = (%s) AND companyID = (%s) AND stockExchangeID = (%s)", [units, portfolioID, companyID, stockExchangeID])
    
    conn.commit()
        
    c.close()
    conn.close()

def editPortfolioName(form):
    portfolioID   = form['portfolioID']
    portfolioName = form['newPortfolioName']
    
    errors = getPortfolioErrors(portfolioName)
    
    if len(errors) == 0:
        c, conn = connection()
        
        c.execute("UPDATE portfolios SET name = (%s) WHERE portfolioID = (%s)", [portfolioName, portfolioID])
        conn.commit()
        
        c.close()
        conn.close()
    
    return errors

def deletePortfolio(form):
    c, conn = connection()
    portfolioID = form['portfolioID']
    
    c.execute("DELETE FROM portfolioCompanies WHERE portfolioID = (%s)", [portfolioID])
    c.execute("DELETE FROM portfolios WHERE portfolioID = (%s)", [portfolioID])
    conn.commit()

    c.close()
    conn.close()

def getPortfolios():
    c, conn = connection()
    doesEntryExist = c.execute("SELECT portfolioID, name FROM portfolios WHERE userID = (%s)", [session['userID']])
    portfolios = c.fetchall()
    
    c.close()
    conn.close()
    
    return portfolios
    
def getPortfolioData(portfolios):
    c, conn = connection()
    
    portfolioData = []
        
    for portfolio in portfolios:
        portfolioRow = []
        
        portfolioID   = portfolio[0]
        portfolioName = portfolio[1]
        
        portfolioRow.append(portfolioID)
        portfolioRow.append(portfolioName)
        
        companiesList = []
        
        portfolioValue = 0
        
        doesEntryExist = c.execute("SELECT companyID, stockExchangeID, units FROM portfolioCompanies WHERE portfolioID = (%s)", [portfolioID])
        companies = c.fetchall()
        
        for company in companies:
            companyList     = []
            companyID       = company[0]
            stockExchangeID = company[1]
            units           = company[2]
            
            c.execute("SELECT companyCode, companyName FROM companies WHERE companyID = (%s)", [companyID])
            companyInfo = c.fetchone()
            companyCode = companyInfo[0]
            companyName = companyInfo[1]
            
            c.execute("SELECT stockExchangeAbbrev FROM stockExchanges WHERE stockExchangeID = (%s)", [stockExchangeID])
            stockExchangeAbbrev = c.fetchone()[0]
            
            doesEntryExist = c.execute("SELECT price FROM liveCompanyData WHERE companyID = (%s) AND stockExchangeID = (%s) ORDER BY liveCompanyDataID DESC;", [companyID, stockExchangeID])
            if int(doesEntryExist) > 0:
                currentPrice = c.fetchone()[0]
            else:
                currentPrice = 0
                
            doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) AND stockExchangeID = (%s) ORDER BY date DESC;", [companyID, stockExchangeID])
            if int(doesEntryExist) > 0:
                previousClose = c.fetchone()[0]

                percentageChange = (currentPrice - previousClose)/(previousClose)*100
                percentageChange = round(percentageChange, 2)
                changeValue      = float(currentPrice - previousClose)
            else:
                previousClose    = 0
                percentageChange = 0
                changeValue      = 0

            if percentageChange > 0:
                color = "green"
            elif percentageChange < 0:
                color = "red"
            else:
                color = "grey"
                
            totalCompanyValue = units * currentPrice
            portfolioValue    = portfolioValue + totalCompanyValue
            
            companyList.append(companyID)
            companyList.append(companyCode)
            companyList.append(companyName)
            companyList.append(stockExchangeID)
            companyList.append(stockExchangeAbbrev)
            companyList.append(units)
            companyList.append(float(currentPrice))
            companyList.append(float(percentageChange))
            companyList.append(float(changeValue))
            companyList.append(color)
            companyList.append(float(totalCompanyValue))
            
            companiesList.append(companyList)
        
        portfolioRow.append(float(portfolioValue))
        portfolioRow.append(companiesList)
        portfolioData.append(portfolioRow)
        
    return portfolioData

#Watchlist Functions

def getWatchListErrors(watchlistName):
    errors = []
    
    
                
    c.execute("SELECT name FROM watchlists WHERE userID = (%s)", [session['userID']])
    existingNames = c.fetchall()
    
    for name in existingNames:
        if name[0] == watchlistName:
            errors.append("Watchlist already exists")
            break
    
    if len(watchlistName) == 0 or len(watchlistName) > 45:
        errors.append("Please enter a name between 1 and 45 characters")
        
    if len(errors) == 0:
        addWatchlist(watchlistName)
        
    return errors
    
def addWatchlist(form):
    watchlistName = form['watchlistName']
    errors = getWatchListErrors(watchlistName)
    
    if len(errors) == 0:
        c, conn = connection()
        
        doesEntryExist = c.execute("INSERT INTO watchlists (userID, name) VALUES (%s, %s)", [session['userID'], watchlistName])
        conn.commit()
        
        c.close()
        conn.close()
    
    return errors

def addCompanyToNewWatchlist(form):
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    userID          = session['userID']
    watchlistName   = form['new_watchlist']

    c, conn = connection()
    
    c.execute("INSERT INTO watchlists (userID, name) VALUES (%s, %s)", [userID, watchlistName])
    conn.commit()
    
    c.execute("SELECT watchlistID FROM watchlists WHERE userID = (%s) AND name = (%s)", [userID, watchlistName])
    watchlistID = c.fetchone()[0]
    
    c.execute("INSERT INTO watchlistCompanies (`watchlistID`, `companyID`, `stockExchangeID`) VALUES (%s, %s, %s)", [watchlistID, companyID, stockExchangeID])
    conn.commit()
    
    c.close()
    conn.close()
    
def addCompanyToWatchlist(form):
    watchlistID     = form['watchlist']
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    userID          = session['userID']
    
    c, conn = connection()
    
    doesEntryExist = c.execute("SELECT watchlistCompanyID FROM watchlistCompanies WHERE watchlistID = %s AND stockExchangeID = (%s) AND companyID = %s", [watchlistID, stockExchangeID, companyID])
    
    if doesEntryExist == 0:
        c.execute("INSERT INTO watchlistCompanies (`watchlistID`, `companyID`, `stockExchangeID`) VALUES (%s, %s, %s)", [watchlistID, companyID, stockExchangeID])
        conn.commit()
        
    c.close()
    conn.close()
        
def deleteWatchlistCompany(form):
    watchlistID     = form['watchlistID']
    stockExchangeID = form['stockExchangeID']
    companyID       = form['companyID']
    
    c, conn = connection()
    
    c.execute("DELETE FROM watchlistCompanies WHERE watchlistID = %s AND stockExchangeID = %s AND companyID = %s", [watchlistID, stockExchangeID, companyID])
    conn.commit()
    
    c.close()
    conn.close()
    
def editWatchlistName(form):
    watchlistID   = form['watchlistID']
    watchlistName = form['newWatchlistName']
    
    c, conn = connection()
    
    c.execute("UPDATE watchlists SET name = (%s) WHERE watchlistID = (%s)", [watchlistName, watchlistID])
    conn.commit()
    
    c.close()
    conn.close()
    
def deleteWatchlist(form):
    watchlistID = form['watchlistID']
    
    c, conn = connection()
    
    c.execute("DELETE FROM watchlistCompanies WHERE watchlistID = (%s)", [watchlistID])
    c.execute("DELETE FROM watchlists WHERE watchlistID = (%s)", [watchlistID])
    conn.commit()
    
    c.close()
    conn.close()
    
def getWatchlists():
    c, conn = connection()
    
    c.execute("SELECT watchlistID, name FROM watchlists WHERE userID = (%s)", [session['userID']])    
    watchlists = c.fetchall()
    
    c.close()
    conn.close()

    return watchlists
    
def getWatchlistData(watchlists):
    c, conn = connection()
    
    watchlistData = []
        
    for watchlist in watchlists:
        watchlistRow = []
        watchlistID   = watchlist[0]
        watchlistName = watchlist[1]
        
        watchlistRow.append(watchlistID)
        watchlistRow.append(watchlistName)
        
        c.execute("SELECT stockExchangeID, companyID FROM watchlistCompanies WHERE watchlistID = (%s)", [watchlistID])
        companies = c.fetchall()
        
        companiesList = []
        
        for company in companies:
            companyList = []
            stockExchangeID = company[0]
            companyID       = company[1]
            
            c.execute("SELECT companyName, companyCode FROM companies WHERE companyID = %s", [companyID])
            companyData = c.fetchone()
            companyName = companyData[0]
            companyCode = companyData[1]
            
            c.execute("SELECT stockExchangeAbbrev FROM stockExchanges WHERE stockExchangeID = %s", [stockExchangeID])
            stockExchangeAbbrev = c.fetchone()[0]
            
            doesEntryExist = c.execute("SELECT price FROM liveCompanyData WHERE companyID = (%s) AND stockExchangeID = (%s) ORDER BY liveCompanyDataID DESC;", [companyID, stockExchangeID])
            if int(doesEntryExist) > 0:
                currentPrice = c.fetchone()[0]
            else:
                currentPrice = 0
                
            doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) AND stockExchangeID = (%s) ORDER BY date DESC;", [companyID, stockExchangeID])
            if int(doesEntryExist) > 0:
                previousClose = c.fetchone()[0]

                percentageChange = (currentPrice - previousClose)/(previousClose)*100
                percentageChange = round(percentageChange, 2)
                changeValue      = float(currentPrice - previousClose)
            else:
                previousClose    = 0
                percentageChange = 0
                changeValue      = 0

            if percentageChange > 0:
                color = "green"
            elif percentageChange < 0:
                color = "red"
            else:
                color = "grey"
                            
            companyList.append(companyID)
            companyList.append(companyCode)
            companyList.append(companyName)
            companyList.append(stockExchangeID)
            companyList.append(stockExchangeAbbrev)
            companyList.append(float(currentPrice))
            companyList.append(float(percentageChange))
            companyList.append(float(changeValue))
            companyList.append(color)
            
            companiesList.append(companyList)
        
        watchlistRow.append(companiesList)
        
        watchlistData.append(watchlistRow)
    
    c.close()
    conn.close()
        
    return watchlistData
        
#Blog Functions

def addBlog(form, username):
    title       = form['title']
    description = form['description']
    
    c, conn = connection()
    
    c.execute("SELECT userID FROM users WHERE username = %s", [username])
    userID = c.fetchone()[0]
    
    c.execute("INSERT INTO blogs (userID, title, description) VALUES (%s, %s, %s)", [userID, title, description])
    
    conn.commit()
    
    c.close()
    conn.close()

def getBlogs(username):
    c, conn = connection()
    
    c.execute("SELECT userID FROM users WHERE username = %s", [username])
    userID = c.fetchone()[0]
    
    c.execute("SELECT title, description FROM blogs WHERE userID = %s", [userID])
    blogs = c.fetchall()
    
    c.close()
    conn.close()
    
    return blogs
    
def addBlogPost(form, blogTitle):
    title       = form['blogTitle']
    description = form['description']
    contents    = form['postContents']
    
    c, conn = connection()
    
    c.execute("SELECT userID FROM users WHERE username = %s", [session["username"]])
    userID = c.fetchone()[0]
    
    c.execute("SELECT blogID FROM blogs WHERE userID = %s and title = %s", [userID, blogTitle])
    blogID = c.fetchone()[0]
    
    c.execute("INSERT INTO blogPosts (blogID, title, description) VALUES (%s, %s, %s)", [blogID, title, description])
    conn.commit()
    
    c.execute("SELECT blogPostID FROM blogPosts WHERE blogID = %s and title = %s", [blogID, title])
    blogPostID = c.fetchone()[0]
    
    c.execute("INSERT INTO blogPostContents (blogPostID, blogPostContents) VALUES (%s, %s)", [blogPostID, contents])
    conn.commit()
    
    c.close()
    conn.close()
    
def getBlogPosts(username, title):
    c, conn = connection()
            
    c.execute("SELECT userID FROM users WHERE username = %s", [username])
    userID = c.fetchone()[0]
    
    c.execute("SELECT blogID FROM blogs WHERE userID = %s and title = %s", [userID, title])
    blogID = c.fetchone()[0]
    
    c.execute("SELECT title, description FROM blogPosts WHERE blogID = %s", [blogID])
    posts = c.fetchall()
    
    c.close()
    conn.close()
    
    return posts
    
def getBlogPost(username, blogTitle, blogPostTitle):
    c, conn = connection()
            
    c.execute("SELECT userID FROM users WHERE username = %s", [username])
    userID = c.fetchone()[0]
    
    c.execute("SELECT blogID FROM blogs WHERE userID = %s and title = %s", [userID, blogTitle])
    blogID = c.fetchone()[0]
    
    c.execute("SELECT blogPostID, title, description FROM blogPosts WHERE blogID = %s AND title = %s", [blogID, blogPostTitle])
    postData = c.fetchone()
    
    post = []
    
    blogPostID  = postData[0]
    title       = postData[1]
    description = postData[2]
    
    c.execute("SELECT blogPostContents FROM blogPostContents WHERE blogPostID = %s", [blogPostID])
    blogPostContents = c.fetchone()[0]
    
    post.append(title)
    post.append(description)
    post.append(blogPostContents)
    
    c.close()
    conn.close()
    
    return post
    
def addBlogComment():
    pass
    
def doesUserExist(username):
    c, conn = connection()
    doesUserExist = c.execute("SELECT userID FROM users WHERE username = %s", [username])
    c.close()
    conn.close()
    if doesUserExist == 0:
        return False
    else:
        return True

def doesBlogExist(username, title):
    c, conn = connection()
    
    doesUserExist = c.execute("SELECT userID FROM users WHERE username = %s", [username])
    
    if doesUserExist == 0:
        return False
    else:
        userID = c.fetchone()[0]
    
    doesBlogExist = c.execute("SELECT blogID FROM blogs WHERE userID = %s AND title = %s", [userID, title])
    
    c.close()
    conn.close()
        
    if doesBlogExist == 0:
        return False
    else:
        return True
        
def doesBlogPostExist(username, blogTitle, blogPostTitle):
    c, conn = connection()
    
    doesUserExist = c.execute("SELECT userID FROM users WHERE username = %s", [username])
    
    if doesUserExist == 0:
        return False
    else:
        userID = c.fetchone()[0]
    
    doesBlogExist = c.execute("SELECT blogID FROM blogs WHERE userID = %s AND title = %s", [userID, blogTitle])
        
    if doesBlogExist == 0:
        return False
    else:
        blogID = c.fetchone()[0]
    
    doesBlogPostExist = c.execute("SELECT blogPostID FROM blogPosts WHERE blogID = %s AND title = %s", [blogID, blogPostTitle])
    
    c.close()
    conn.close()
    
    if doesBlogPostExist == 0:
        return False
    else:
        return True
        
#Settings Functions

def changePassword(form):
    c, conn = connection()
    
    errors = []
    
    c.execute("SELECT password FROM users WHERE userID = (%s)", [session['userID']])
    password        = c.fetchone()[0]
    currentPassword = form['currentPassword']
    newPassword     = form['newPassword']
    confirmPassword = form['confirmPassword']
    
    if not currentPassword:
        errors.append('Please enter your password')
    elif not newPassword:
        errors.append('Please enter a new password')
    elif not confirmPassword:
        errors.append('Please confirm new password')
    elif sha256_crypt.verify(currentPassword, password) == False:
        errors.append('Incorrect password')
    elif newPassword != confirmPassword:
        errors.append('Passwords do not match')
    
    if len(errors) == 0:
        newPassword = sha256_crypt.encrypt(str(newPassword))
        c.execute("UPDATE users SET password = (%s) WHERE userID = (%s)", [newPassword, session['userID']])
        conn.commit()
        
    c.close()
    conn.close()
    
    return errors
    
def checkURL(url):
    if url[:22] == "http://128.199.224.150":
        pass
    