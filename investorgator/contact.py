from investorgator import app
from flask import Flask, request, render_template, redirect, url_for

@app.route('/contact')
def contact():
    return render_template("contact.html")