from investorgator.connection import connection
from flask import session

def getIndexPercentageChanges():
    c, conn = connection()
    
    indexPercentageChanges = []
    
    #AUS
    doesEntryExist = c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = 'XAO'")
    if int(doesEntryExist) > 0:
        asxID = c.fetchone()[0]
        
        c.execute("SELECT close, date FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY date DESC;", [asxID])
        asxRecentCloses  = c.fetchall()
        asxLastClose     = asxRecentCloses[0][0]
        asxPreviousClose = asxRecentCloses[1][0]
        
        asx_change = (asxLastClose - asxPreviousClose)/(asxPreviousClose)*100        
        
        indexPercentageChanges.append(asx_change)
    
    
    #USA
    
    '''
        c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = ''")
        nasdaqID = c.fetchone()[0]
        c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = ''")
        nyseID = c.fetchone()[0]
        '''
        
    '''
        c.execute("SELECT close, date FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY date DESC;", [nasdaqID])
        nasdaqRecentCloses  = c.fetchall()
        nasdaqLastClose     = nasdaqRecentCloses[0][0]
        nasdaqPreviousClose = nasdaqRecentCloses[1][0]
        
        c.execute("SELECT close, date FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY date DESC;", [asxID])
        asxRecentCloses  = c.fetchall()
        asxLastClose     = asxRecentCloses[0][0]
        asxPreviousClose = asxRecentCloses[1][0]
        '''
    
    c.close()
    conn.close()
    
    return indexPercentageChanges

def getStockExchanges():
    c, conn = connection()
    
    c.execute("SELECT stockExchangeName, stockExchangeCode, stockExchangeAbbrev FROM stockExchanges")
    stockExchanges = c.fetchall()
    
    c.close()
    conn.close()

    return stockExchanges

def getAllCompanies():
    c, conn = connection()
    c.execute("SELECT stockExchangeID, companyName, companyCode, industry FROM companies")
    companies = c.fetchall()
    
    companyList = []
    
    for company in companies:
        companyRow = []
        c.execute("SELECT stockExchangeAbbrev FROM stockExchanges WHERE stockExchangeID = (%s)", [company[0]])
        companyRow.append(company[1])
        companyRow.append(company[2])
        companyRow.append(company[3])
        companyRow.append(c.fetchone()[0])
        companyList.append(companyRow)
    
    companies = companyList
    
    c.close()
    conn.close()

    return companies

def getStockExchangeID(stockExchange):
    c, conn = connection()

    doesStockExchangeExist = c.execute("SELECT stockExchangeID  FROM stockExchanges WHERE stockExchangeAbbrev = (%s)", [stockExchange])
    
    if int(doesStockExchangeExist) == 0:
        c.close()
        conn.close()
        return False, None
    else:
        stockExchangeID = c.fetchone()[0]
        c.close()
        conn.close()
        return True, stockExchangeID

def getStockExchangeName(stockExchangeID):
    c, conn = connection()

    c.execute("SELECT stockExchangeName FROM stockExchanges WHERE stockExchangeID= (%s)", [stockExchangeID])
    stockExchangeName = c.fetchone()[0]
    
    c.close()
    conn.close()

    return stockExchangeName
        
def getStockExchangeCode(stockExchangeID):
    c, conn = connection()

    c.execute("SELECT stockExchangeCode FROM stockExchanges WHERE stockExchangeID= (%s)", [stockExchangeID])
    stockExchangeCode = c.fetchone()[0]
    
    c.close()
    conn.close()

    return stockExchangeCode
    
def getMainStockExchangeIndexID(stockExchangeAbbrev, stockExchangeID):
    c, conn = connection()
    
    if stockExchangeAbbrev == 'ASX':
        indexCode = 'XAO'
    
    c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = (%s) AND stockExchangeID = (%s)", [indexCode, stockExchangeID])
    
    stockExchangeIndexID = c.fetchone()[0]
    
    c.close()
    conn.close()
    
    return stockExchangeIndexID

def getAllStockExchangeCompaniesData(stockExchangeID):
    c, conn = connection()

    c.execute("SELECT companyID, companyName, companyCode FROM companies WHERE stockExchangeID = (%s)", [stockExchangeID])
    companies = c.fetchall()

    companiesList = []
    
    for company in companies:
        companyInfo = []
        
        doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY historicalCompanyDataID DESC;", [company[0]])
        
        if doesEntryExist > 0:
            close = c.fetchone()[0]
        else:
            close = 0
        
        for item in company:
            companyInfo.append(item)
            
        companyInfo.append(close)
        companiesList.append(companyInfo)
    
    c.close()
    conn.close()

    return companiesList

def getIndiciesList(stockExchangeID):
    c, conn = connection()

    c.execute("SELECT stockExchangeIndexID, indexName, indexCode FROM stockExchangeIndicies WHERE stockExchangeID = (%s)", [stockExchangeID])
    indicies = c.fetchall()

    indiciesList = []
    
    for index in indicies:
        indexInfo = []
        doesEntryExist = c.execute("SELECT close FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY historicalIndexDataID DESC;", [index[0]])
        if doesEntryExist > 0:
            close = c.fetchone()[0]
        else:
            close = 0
        
        for item in index:
            indexInfo.append(item)
            
        indexInfo.append(close)
        indiciesList.append(indexInfo)
        
    indicies = indiciesList
        
    c.close()
    conn.close()

    return indiciesList


def getStockExchangeIndexID(indexCode, stockExchangeID):
    c, conn = connection()

    doesStockExchangeIndexExist = c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = (%s) AND stockExchangeID = (%s)", [indexCode, stockExchangeID])
    
    if int(doesStockExchangeIndexExist) == 0:
        c.close()
        conn.close()
        return False, False
    else:
        stockExchangeIndexID = c.fetchone()[0]
        c.close()
        conn.close()
        return True, stockExchangeIndexID
        
def getStockExchangeIndexName(stockExchangeIndexID):
    c, conn = connection()

    doesStockExchangeIndexExist = c.execute("SELECT indexName FROM stockExchangeIndicies WHERE stockExchangeIndexID = (%s)", [stockExchangeIndexID])
    
    indexName = c.fetchone()[0]
    
    c.close()
    conn.close()
    return indexName
    
def getCurrentStockExchangeIndexPrice(stockExchangeIndexID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY date DESC;", [stockExchangeIndexID])
    if doesEntryExist >= 1:
        currentPrice = c.fetchone()[0]
    else:
        currentPrice = 0

    c.close()
    conn.close()

    return currentPrice

def getPreviousStockExchangeIndexClosePrice(stockExchangeIndexID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalIndexData WHERE stockExchangeIndexID = (%s) ORDER BY date DESC;", [stockExchangeIndexID])
    
    if doesEntryExist >= 2:
        PreviousStockExchangeIndexClosePrice = c.fetchall()[1][0]
    else:
        PreviousStockExchangeIndexClosePrice = 0

    c.close()
    conn.close()

    return PreviousStockExchangeIndexClosePrice

def getHistoricalIndexPrices(stockExchangeIndexID):
    c, conn = connection()

    c.execute("SELECT date, close FROM historicalIndexData WHERE stockExchangeIndexID = (%s)", [stockExchangeIndexID])
    priceData = c.fetchall()

    c.close()
    conn.close()

    return priceData
        
def getGoogleIndexChartData(stockExchangeIndexID):
    c, conn = connection()

    priceData = getHistoricalIndexPrices(stockExchangeIndexID)
    googleChartData = []
    
    for row in priceData:
        chartRow = []
        chartRow.append(str(row[0])[0:4])
        chartRow.append(str(row[0])[5:7])
        chartRow.append(str(row[0])[8:10])
        chartRow.append(row[1])
        googleChartData.append(chartRow)

    c.close()
    conn.close()

    return googleChartData
        
def getHistroicalIndexData(stockExchangeIndexID, stockExchangeID):
    c, conn = connection()

    c.execute("SELECT date, open, high, low, close, adjustedClose FROM historicalIndexData WHERE stockExchangeIndexID = (%s) AND stockExchangeID = (%s) ORDER BY date DESC;", [stockExchangeIndexID, stockExchangeID])

    historicalIndexData = c.fetchall()
        
    c.close()
    conn.close()

    return historicalIndexData



    
    
    
    
    
    
    
    
def getCompanyID(companyCode, stockExchangeID):
    c, conn = connection()

    doesCompanyExist = c.execute("SELECT companyID FROM companies WHERE companyCode = (%s) AND stockExchangeID = (%s)", [companyCode, stockExchangeID])
    
    if int(doesCompanyExist) == 0:
        c.close()
        conn.close()
        return False, None
    else:
        companyID   = c.fetchone()[0]
        c.close()
        conn.close()
        return True, companyID

def getCompanyName(companyID):
    c, conn = connection()
    
    c.execute("SELECT companyName FROM companies WHERE companyID = (%s)", [companyID])
    companyName = c.fetchone()[0]

    c.close()
    conn.close()
    
    return companyName

def getCompanyIndustry(companyID):
    c, conn = connection()
    
    c.execute("SELECT companyName FROM companies WHERE companyID = (%s)", [companyID])
    industry = c.fetchone()[0]

    c.close()
    conn.close()

    return industry

def getCurrentCompanyPrice(companyID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])
    if doesEntryExist >= 2:
        currentPrice = c.fetchone()[0]
    else:
        currentPrice = 0

    c.close()
    conn.close()

    return currentPrice

def getPreviousCompanyClosePrice(companyID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])
    
    if doesEntryExist >= 2:
        previousPrice = c.fetchall()[1][0]
    else:
        previousPrice = 0

    c.close()
    conn.close()

    return previousPrice

def getPercentageChange(currentPrice, previousPrice):
    if previousPrice == 0:
        percentageChange = 0
    else:
        percentageChange = (currentPrice - previousPrice)/(previousPrice)*100
        percentageChange = round(percentageChange, 2)

    return percentageChange

def getCompanyPercentageChange(companyID):
    c, conn = connection()
    
    c.execute("SELECT percentageChange FROM companies WHERE companyID = (%s)", [companyID])
    companyPercentageChange = c.fetchone()[0]
    
    c.close()
    conn.close()
    return companyPercentageChange
    
def getChangeValue(currentPrice, previousPrice): 
    changeValue = float(currentPrice - previousPrice)
    return changeValue

def getPercentageChangeColor(percentageChange):
    if percentageChange > 0:
        color = "green"
    elif percentageChange < 0:
        color = "red"
    else:
        color = "grey"

    return color
    
def getPortfolios():   
    portfolios = []
    
    if session.get('logged_in'):
        c, conn = connection()

        c.execute("SELECT portfolioID, name FROM portfolios WHERE userID = (%s)", [session['userID']])
        portfolios = c.fetchall()

        c.close()
        conn.close()

    return portfolios

def getWatchlists():    
    

    watchlists = []

    if session.get('logged_in'):
        c, conn = connection()
        
        c.execute("SELECT watchlistID, name FROM watchlists WHERE userID = (%s)", [session['userID']])
        watchlists = c.fetchall()
    
        c.close()
        conn.close()
    
    return watchlists

def getHistoricalCompanyPrices(companyID):
    c, conn = connection()

    c.execute("SELECT date, close FROM historicalCompanyData WHERE companyID = (%s)", [companyID])
    priceData = c.fetchall()

    c.close()
    conn.close()

    return priceData

def getLiveCompanyPrices(companyID):
    c, conn = connection()

    c.execute("SELECT date, time, price FROM liveCompanyData WHERE companyID = (%s)", [companyID])
    priceData = c.fetchall()

    c.close()
    conn.close()

    return priceData


def getGoogleCompanyChartData(companyID):
    c, conn = connection()

    priceData = getHistoricalCompanyPrices(companyID)
    googleChartData = []
    
    for row in priceData:
        chartRow = []
        chartRow.append(str(row[0])[0:4])
        chartRow.append(str(row[0])[5:7])
        chartRow.append(str(row[0])[8:10])
        chartRow.append(row[1])
        googleChartData.append(chartRow)

    c.close()
    conn.close()

    return googleChartData
 
def getLiveGoogleCompanyChartData(companyID):
    c, conn = connection()

    priceData = getLiveCompanyPrices(companyID)
    liveGoogleChartData = []
    
    for row in priceData:
        chartRow = []
        
        if row[0][1] == '/' and row[0][3] == '/':
            chartRow.append(row[0][4:8])
            chartRow.append(row[0][0])
            chartRow.append(row[0][2])
        elif row[0][2] == '/' and row[0][4] == '/':
            chartRow.append(row[0][5:9])
            chartRow.append(row[0][0:2])
            chartRow.append(row[0][3])
        elif row[0][1] == '/' and row[0][4] == '/':
            chartRow.append(row[0][5:9])
            chartRow.append(row[0][0])
            chartRow.append(row[0][2:4])
        elif row[0][2] == '/' and row[0][5] == '/':
            chartRow.append(row[0][6:10])
            chartRow.append(row[0][0:2])
            chartRow.append(row[0][3:5])
        
        if str(row[1])[1] == ':':
            hour = row[1][0]
            minute = row[1][2:4]
        elif row[1][2] == ':':
            hour = row[1][0:2]
            minute = row[1][3:5]
            
        chartRow.append(hour)
        chartRow.append(minute)
        chartRow.append(row[2])
        liveGoogleChartData.append(chartRow)
                
    c.close()
    conn.close()

    return liveGoogleChartData

def getHistoricalCompanyPriceData(companyID):
    c, conn = connection()

    c.execute("SELECT date, open, high, low, close, volume, adjustedClose FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])

    historicalCompanyData = c.fetchall()

    c.close()
    conn.close()

    return historicalCompanyData
    
def getStockExchangeAdvancers(stockExchangeID):
    c, conn = connection()
    
    #
    c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])
    historicalCompanyData = c.fetchall()

    c.close()
    conn.close()

    return historicalCompanyData
    
def getTopVolumeCompanies(stockExchangeID):
    c, conn = connection()
    
    #get date
    c.execute("SELECT date FROM historicalCompanyData WHERE stockExchangeID = (%s) ORDER BY date DESC LIMIT 1;", [stockExchangeID])
    date = c.fetchone()[0]
    
    c.execute("SELECT companyID, volume, close FROM historicalCompanyData WHERE stockExchangeID = (%s) AND date = %s ORDER BY volume DESC LIMIT 5;", [stockExchangeID, date])
    companyData = c.fetchall()
    
    topVolumeCompanies = []
    
    if len(companyData) >= 5:
        for company in companyData:
            topVolumeCompaniesRow = []
            
            companyID = company[0]
            volume    = company[1]
            close     = company[2]
            
            c.execute("SELECT companyName FROM companies WHERE companyID = (%s)", [companyID])
            companyName = c.fetchone()[0]
            
            topVolumeCompaniesRow.append(companyName)
            topVolumeCompaniesRow.append(volume)
            topVolumeCompaniesRow.append(close)
            
            topVolumeCompanies.append(topVolumeCompaniesRow)
    
    c.close()
    conn.close()

    return topVolumeCompanies
    
def getTopAdvancerCompanies(stockExchangeID):
    c, conn = connection()
    
    c.execute("SELECT companyID FROM companies WHERE stockExchangeID = (%s)", [stockExchangeID])
    companies = c.fetchall()
    
    c.execute("SELECT companyID, companyName, percentageChange FROM companies WHERE stockExchangeID = (%s) ORDER BY percentageChange DESC LIMIT 5;", [stockExchangeID])
    companies = c.fetchall()
    
    topAdvancers = []
    
    for company in companies:
        topAdvancersRow = []
        companyName      = company[1]
        percentageChange = company[2]
        color = getPercentageChangeColor(percentageChange)
        
        topAdvancersRow.append(companyName)
        topAdvancersRow.append(percentageChange)
        topAdvancersRow.append(color)
        
        topAdvancers.append(topAdvancersRow)
    
    c.close()
    conn.close()

    return topAdvancers

def getTopDeclinerCompanies(stockExchangeID):
    c, conn = connection()
    
    c.execute("SELECT companyID FROM companies WHERE stockExchangeID = (%s)", [stockExchangeID])
    companies = c.fetchall()
    
    c.execute("SELECT companyID, companyName, percentageChange FROM companies WHERE stockExchangeID = (%s) ORDER BY percentageChange ASC LIMIT 5;", [stockExchangeID])
    companies = c.fetchall()
    
    topDecliners = []
    
    for company in companies:
        topDeclinersRow = []
        companyName      = company[1]
        percentageChange = company[2]
        color = getPercentageChangeColor(percentageChange)
        
        topDeclinersRow.append(companyName)
        topDeclinersRow.append(percentageChange)
        topDeclinersRow.append(color)
        
        topDecliners.append(topDeclinersRow)
    
    c.close()
    conn.close()

    return topDecliners
    
def getMajorIndicies(stockExchangeID):
    c.execute("SELECT stockExchangeCode FROM stockExchanges WHERE stockExchangeID = (%s)", [stockExchangeID])
    stockExchangeCode = c.fetchone()[0]
    
    if stockExchangeCode == 'AX':
        majorIndicies = []