from investorgator import app
from investorgator.functionsStockExchanges import *
from flask import Flask, request, render_template, redirect, url_for, session
from investorgator.connection import connection

#Main stock exchanges page
@app.route('/investing_simulator', methods=['GET'])
@app.route('/investing_simulator/', methods=['GET'])
def investing_simulator():
    
    return render_template("investing_simulator/investing_simulator.html")