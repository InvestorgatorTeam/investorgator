from investorgator import app
from investorgator.connection import connection
from investorgator.functionsUsers import *
from flask import Flask, render_template, redirect, session, url_for, request, flash
from passlib.hash import sha256_crypt
import os

@app.route('/users', methods=['GET'])
@app.route('/users/', methods=['GET'])
def users():
    return render_template("users/users.html")

'''
@app.route('/users/find_user', methods=['GET'])
@app.route('/users/find_user/', methods=['GET'])
def find_user():
    return render_template("index.html")
'''
 
@app.route('/users/<username>', methods=['GET'])
@app.route('/users/<username>/', methods=['GET'])
def user(username = False):
    if username == False:
        return redirect(url_for('index'))
    else:
        if username == session.get('username'):
            return render_template("users/my_profile.html", username = username)
        else:
            if doesUserExist(username) == True:
                return render_template("users/profile.html", username = username)
            else:
                return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
@app.route('/register/', methods=['GET', 'POST'])
def register():
    if session.get('logged_in'):
        if session['logged_in'] == True:
            return redirect(url_for("index"))
        
    errors = []
    
    if request.method == 'POST':
        if request.form['form'] == "registerLink":
            session['previousURL'] = request.form['previousURL']
            return redirect(url_for("register"))
        elif request.form['form'] == "register":
            errors = registerErrorCheck(request.form)
            if len(errors) == 0:
                registerUser(request.form)
                if session.get('previousURL') is not None:
                    url = session['previousURL']
                else:
                    url = "/"
                return redirect(url)
    
    return render_template("users/register.html", errors = errors)
    
@app.route('/login', methods=['GET', 'POST'])
@app.route('/login/', methods=['GET', 'POST'])
def login():
    if session.get('logged_in'):
        if session['logged_in'] == True:
            return redirect(url_for("index"))
        
    errors = []
    if request.method == 'POST':
        if request.form['form'] == "loginLink":
            session['previousURL'] = request.form['previousURL']
            return redirect(url_for("login"))
        elif request.form['form'] == "login":
            errors, userInfo = getLoginErrors(request.form)
            if len(errors) == 0:
                loginUser(userInfo)
                if session.get('previousURL') is not None:
                    url = session['previousURL']
                else:
                    url = "/"
                return redirect(url)

    return render_template("users/login.html", errors = errors)
        
@app.route('/logout', methods=['GET', 'POST'])
@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    if request.method == "POST":
        url = request.form['previousURL']
        logoutUser()
        return redirect(url)
    
    logoutUser()
    return redirect(url_for('index'))
    
@app.route('/users/<username>/portfolios', methods=['GET', 'POST'])
@app.route('/users/<username>/portfolios/', methods=['GET', 'POST'])
def portfolio(username=False):
    if username == session.get('username'):
        errors = []        
        if request.method == 'POST':
            if request.form['form'] == "addPortfolio":
                errors = addPortfolio(request.form)
                if len(errors) == 0:
                    return redirect("/users/" + session['username'] + "/portfolios")
                    
            elif request.form['form'] == "addCompany":                
                if checkAddCompany(request.form):
                    return redirect(previousURL)
                elif request.form['portfolio'] == "new_portfolio":
                    if len(request.form['new_portfolio']) > 0:
                        addCompanyToNewPortfolio(request.form)
                    else:
                        return redirect(request.form['previousURL'])
                else:
                    addCompanyToPortfolio(request.form)

                return redirect("/users/" + session['username'] + "/portfolios")
                
            elif request.form['form'] == "deletePortfolioCompany":
                deletePortfolioCompany(request.form)
                return redirect("/users/" + session['username'] + "/portfolios")
                
            elif request.form['form'] == "editCompanyUnits":
                editCompanyUnits(request.form)
                return redirect("/users/" + username + "/portfolios")
                
            elif request.form['form'] == "editPortfolioName":
                errors = editPortfolioName(request.form)
                if len(errors) == 0:
                    return redirect("/users/" + session['username'] + "/portfolios")
                
            elif request.form['form'] == "deletePortfolio":
                deletePortfolio(request.form)
                return redirect("/users/" + username + "/portfolios")
        
        portfolios = getPortfolios()
        portfolioData = getPortfolioData(portfolios)
        return render_template('users/portfolios.html', username = username, portfolios = portfolios, portfolioData = portfolioData, errors = errors)
    else:
        return redirect(url_for('login'))

@app.route('/users/<username>/watchlists', methods=['GET', 'POST'])
@app.route('/users/<username>/watchlists/', methods=['GET', 'POST'])
def watchlists(username=False):
    if username == session.get('username'):
        errors = []
        c, conn = connection()
        if request.method == 'POST':
            if request.form['form'] == "addWatchlist":
                errors = addWatchlist(request.form)
                if len(errors) == 0:
                    return redirect("/users/" + username + "/watchlists")
            
            elif request.form['form'] == "addCompany":
                if request.form['watchlist'] == "new_watchlist":
                    if len(request.form['new_watchlist']) > 0:
                        addCompanyToNewWatchlist(request.form)
                    else:
                        return redirect(request.form['previousURL'])
                else:
                    addCompanyToWatchlist(request.form)
                
                return redirect("/users/" + username + "/watchlists")
            
            elif request.form['form'] == "deleteCompany":
                deleteWatchlistCompany(request.form)
                return redirect("/users/" + username + "/watchlists")
                
            elif request.form['form'] == "editWatchlistName":
                editWatchlistName(request.form)
                return redirect("/users/" + username + "/watchlists")
                
            elif request.form['form'] == "deleteWatchlist":
                deleteWatchlist(request.form)
                return redirect("/users/" + username + "/watchlists")
                            
        watchlists = getWatchlists()
        watchlistData = getWatchlistData(watchlists)
        
        return render_template('users/watchlists.html',
                                username = username,
                                watchlists = watchlists,
                                errors = errors,
                                watchlistData = watchlistData
        )
    else:
        return redirect(url_for('index'))

@app.route('/users/<username>/blogs', methods=['GET', 'POST'])
@app.route('/users/<username>/blogs/', methods=['GET', 'POST'])               
def blogs(username = False):
    if request.method == 'POST':
        if request.form['form'] == 'addBlog':
            addBlog(request.form, username)
            return redirect(url_for('blog', username = username, title = request.form['title']))

    if doesUserExist(username):
        blogs = getBlogs(username)
        return render_template("users/blogs.html",
                                username = username,
                                blogs    = blogs
        )
    else:
        return redirect(url_for('index'))
                
@app.route('/users/<username>/blogs/<title>', methods=['GET', 'POST'])
@app.route('/users/<username>/blogs/<title>/', methods=['GET', 'POST'])
def blog(username = False, title = False):
    if request.method == 'POST':
        if request.form['form'] == 'addBlogPost':
            if session['username'] == username:
                addBlogPost(request.form, title)
                return redirect("/users/" + username + "/blogs/" + title + "/" + request.form['blogTitle'])

    if doesBlogExist(username, title):
        posts = getBlogPosts(username, title)
        return render_template("users/blog.html",
                                username  = username,
                                blogTitle = title,
                                posts     = posts
        )
    else:
        return redirect(url_for('index'))
            
@app.route('/users/<username>/blogs/<blogTitle>/<blogPostTitle>',  methods=['GET', 'POST'])
@app.route('/users/<username>/blogs/<blogTitle>/<blogPostTitle>/', methods=['GET', 'POST'])               
def blog_post(username = False, blogTitle = False, blogPostTitle = False):
    if request.method == 'POST':
        if request.form['form'] == 'addComment':
            addBlogComment()
            return redirect(url_for("blog_post", username = username, blogTitle = blogTitle, blogPostTitle = blogPostTitle))
    if doesBlogPostExist(username, blogTitle, blogPostTitle) == True:
        post = getBlogPost(username, blogTitle, blogPostTitle)
        return render_template("users/blog_post.html",
                                username      = username,
                                blogTitle     = blogTitle,
                                blogPostTitle = blogPostTitle,
                                post          = post
        )
    else:
        return redirect(url_for('index'))

@app.route('/users/<username>/settings', methods=['GET', 'POST'])
@app.route('/users/<username>/settings/', methods=['GET', 'POST'])
def settings(username=False):
    if username == session.get('username'):
        errors = []
        if request.method == 'POST':
            if request.form['setting'] == 'change password':
                errors = changePassword(request.form)
                if len(errors) == 0:
                    return redirect(url_for('index'))
        
        return render_template('users/settings.html', username = username, errors = errors)
    else:
        return redirect(url_for('index'))