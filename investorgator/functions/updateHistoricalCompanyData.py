#Update the historical data for indicies given the stock exchange
from dataMiner import *
from dataScraper import *

import sys

if __name__ == "__main__":
    if len(sys.argv) == 2:
        stockExchangeCode = sys.argv[1]
        companies = readCompanyList(stockExchangeCode)
        
        print("Updating Historical Company Data for " + stockExchangeCode + "\n")
        
        for row in companies:
            companyName = row[0]
            companyCode = row[1]
            industry = row[2]
            
            print("Downloading latest data for " + companyName)
            downloadHistoricalCompanyDataCSV(stockExchangeCode, companyCode)
            
            print("Updating data for " + companyName + " in the database")
            updateHistoricalCompanyData(stockExchangeCode, companyCode)
            
            print("Update Successful\n")
                    
        print("Finished updating company historical data")
    else:
        #Add error to log file
        print("Please enter the argument for the desired stock exchange")