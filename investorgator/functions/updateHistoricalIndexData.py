#Update the historical data for indicies given the stock exchange
from dataMiner import *
from dataScraper import *
import sys

if __name__ == "__main__":
    if len(sys.argv) == 2:
        stockExchangeCode = sys.argv[1]
        indicies = getIndexList(stockExchangeCode)
        
        print("Updating Historical Index Data for " + stockExchangeCode + "\n")
        
        for index in indicies:
            indexName = index[0]
            indexCode = index[1]
            scraperCode = index[2]
            
            print("Downloading latest data for " + indexName)
            downloadHistoricalIndexDataCSV(stockExchangeCode, indexCode, scraperCode)
            
            print("Updating data for " + indexName + " in the database")
            updateHistoricalIndexData(stockExchangeCode, indexCode)
            
            print("Update Successful\n")
                    
        print("Finished updating indicies historical data")
    else:
        #Add error to log file
        print("Please enter the argument for the desired stock exchange")