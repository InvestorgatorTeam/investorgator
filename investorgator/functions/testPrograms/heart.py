# The suggestion algorithm
# Created 19th September 2016

from sweepFunctions import *

currentPrices = {}
counter = 0

print("Updating ASX codes")
#updateCodes("ASX")

print("Getting all the asx codes...")
codes = readCodes("ASX")
length = len(codes)
print("Got all the codes\nGetting share data...")
for companyCode in codes:
    counter+=1
    currentPrices[companyCode] = getShareData(companyCode)
    print(counter, "/", length, " Share Price of " + companyCode + " is: " + getShareData(companyCode))
print("Got share data:\n\n")
print(currentPrices)