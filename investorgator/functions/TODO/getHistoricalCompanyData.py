from dataMiner import *

if __name__ == "__main__":
    #Get array of company codes
    companies = readCodes("ASX")

    #Insert all the historical data from the csv files
    for row in companies:
        insertHistoricalDta(row[1], "AX")
        
    print("Finished getting all data")