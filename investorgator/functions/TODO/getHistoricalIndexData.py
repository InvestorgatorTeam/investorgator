from dataMiner import *

if __name__ == "__main__":
    indicies = getIndexValues("ASX")
    
    for row in indicies:
        print("Updating data from " + row[0] + ".csv to database")
        insertIndex("AX", row[0], row[1], row[2])
        
    print("Finished updating indicies historical data")