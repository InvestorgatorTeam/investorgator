from connection import connection
from dataMiner import *
import sys

def getCurrentCompanyPrice(companyID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])
    if doesEntryExist >= 2:
        currentPrice = c.fetchone()[0]
    else:
        currentPrice = 0

    c.close()
    conn.close()

    return currentPrice

def getPreviousCompanyClosePrice(companyID):
    c, conn = connection()

    doesEntryExist = c.execute("SELECT close FROM historicalCompanyData WHERE companyID = (%s) ORDER BY date DESC;", [companyID])
    
    if doesEntryExist >= 2:
        previousPrice = c.fetchall()[1][0]
    else:
        previousPrice = 0

    c.close()
    conn.close()

    return previousPrice

def getPercentageChange(currentPrice, previousPrice):
    if previousPrice == 0:
        percentageChange = 0
    else:
        percentageChange = (currentPrice - previousPrice)/(previousPrice)*100
        percentageChange = round(percentageChange, 2)

    return percentageChange

def updateCompanyPercentageChange(companyID, percentageChange):
    c, conn = connection()
    
    c.execute("UPDATE companies SET percentageChange = %s WHERE companyID = %s", [percentageChange, companyID])
    conn.commit()

    c.close()
    conn.close()

if __name__ == "__main__":
    if len(sys.argv) == 2:
        stockExchangeCode = sys.argv[1]
        companies = readCompanyList(stockExchangeCode)
        
        c, conn = connection()
        
        c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
        stockExchangeID = c.fetchone()[0]
        
        print("Updating Percentage Change Company Data for " + stockExchangeCode + "\n")
        
        for row in companies:
            companyName = row[0]
            companyCode = row[1]
            industry = row[2]
    
            c.execute("SELECT companyID FROM companies WHERE companyCode = %s AND stockExchangeID = %s", [companyCode, stockExchangeID])
            companyID = c.fetchone()[0]
            
            currentPrice  = getCurrentCompanyPrice(companyID)
            previousPrice = getPreviousCompanyClosePrice(companyID)
            
            if currentPrice != 0 and previousPrice != 0:
                #Calculate percentage change
                percentageChange = getPercentageChange(currentPrice, previousPrice)
                
                print(percentageChange)
                
                print("Updating data for " + companyName + " in the database")
                updateCompanyPercentageChange(companyID, percentageChange)
            
                print("Update Successful\n")
            else:
                print("Price = 0\n")
        
        c.close()
        conn.close()
        
        print("Finished updating company historical data")