#Functions to use the scraped data and organise it in the database
#functions for sweeping websites
from connection import connection
import urllib.request
from urllib.request import urlopen
import csv





#function to download listed company data (name, code, industry) into a csv file
def updateCompanyList(stockExchangeCode):
    url = 'http://www.asx.com.au/asx/research/ASXListedCompanies.csv'
    urllib.request.urlretrieve(url, "csv/stockExchanges/listedCompanies/" + stockExchangeCode + ".csv")
    #TO DO - delete header from file / copy it to another file
    #        as it stores time of update and other info

        
        
        
        
        
#Read codes from csv file and return array
def readCompanyList(stockExchangeCode):
    companies = []
    with open("../static/csv/stockExchanges/listedCompanies/" + stockExchangeCode + ".csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        for row in readCSV:
            companies.append(row)
    return companies
    

#Read codes from csv file and return array
def getStockExchangeList():
    stockExchanges = []
    with open("../static/csv/stockExchanges/stockExchanges.csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        for row in readCSV:
            stockExchanges.append(row)
    return stockExchanges
        
        
#Enters all the stock exchanges into the database
def insertStockExchanges():
    print("== Making stock exchange entries ==")
    stockExchanges = []
    with open("../static/csv/stockExchanges/stockExchanges.csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        for row in readCSV:
            stockExchanges.append(row)

    c, conn = connection()

    for stockExchange in stockExchanges:
        #check if already in the database
        stockExchangeName = stockExchange[0]
        stockExchangeCode = stockExchange[1]
        stockExchangeAbbrev = stockExchange[2]
        
        doesStockExchangeExist = c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = (%s)", [stockExchangeCode])
        
        if int(doesStockExchangeExist) < 1:
            print("Adding " + stockExchangeName + " to the database")
            c.execute("INSERT INTO stockExchanges (stockExchangeName, stockExchangeCode, stockExchangeAbbrev) VALUES (%s, %s, %s)", [stockExchangeName, stockExchangeCode, stockExchangeAbbrev])
            conn.commit()
        else:
            print(stockExchangeName + " Already In The Database")
            
    c.close()
    conn.close()
    print("Success\n")

    
    
    
#inserts all the companies into the database
def insertCompany(stockExchangeCode, companyName, companyCode, industry, c, conn):
    c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
    stockExchangeID = c.fetchone()[0]
        
    doesCompanyExist = c.execute("SELECT companyID FROM companies WHERE stockExchangeID = (%s) AND companyCode = (%s)", [stockExchangeID, companyCode])
    
    if int(doesCompanyExist) < 1:
        print("Inserting Into Database")
        c.execute("INSERT INTO companies (stockExchangeID, companyName, companyCode, industry) VALUES (%s, %s, %s, %s)", (stockExchangeID, companyName, companyCode, industry))
    else:
        print("Already In The Database")

    conn.commit()

    
    
    
#Inserts all the historical company data from the csv files into the database
def insertHistoricalCompanyData(stockExchangeCode, companyCode, stockExchangeID, companyID, c, conn):
    try:
        with open("../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv") as csvFile:
            readCSV = csv.reader(csvFile, delimiter=',')
            next(readCSV)
            
            for row in list(readCSV):
                latestDate = row[0]
                break
        
        doesLastDateExist = c.execute("SELECT date FROM historicalCompanyData WHERE companyID = %s ORDER BY date DESC;", [companyID])
        
        if int(doesLastDateExist) > 0:
            lastDate = c.fetchone()[0]
        else:
            lastDate = "No Date"
             
        with open("../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv") as csvFile:
            readCSV = csv.reader(csvFile, delimiter=',')
            next(readCSV)
            
            if str(lastDate) == str(latestDate):
                print("Already Up To Date")
            else:
                print("Updating Data")
                for row in reversed(list(readCSV)):
                    date            = row[0]
                    openPrice       = float(row[1])
                    high            = float(row[2])
                    low             = float(row[3])
                    close           = float(row[4])
                    volume          = int(row[5])
                    adjustedClose   = float(row[6])

                    doesEntryExist = c.execute("SELECT historicalCompanyDataID FROM historicalCompanyData WHERE stockExchangeID = (%s) AND companyID = (%s) AND date = (%s)", [stockExchangeID, companyID, date])
                    if int(doesEntryExist) < 1:
                        c.execute("INSERT INTO historicalCompanyData (companyID, stockExchangeID, date, open, high, low, close, volume, adjustedClose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                (companyID, stockExchangeID, date, openPrice, high, low, close, volume, adjustedClose)
                        )
                        conn.commit()
                        
    except Exception as e:
        print(e)
        print("No data for this company")
    
    
    
#Updates the historical data for all companies
def updateHistoricalCompanyData(stockExchangeCode, companyCode):
    c, conn = connection()
    
    with open("../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        next(readCSV)
        
        for row in readCSV:
            date            = row[0]
            openPrice       = float(row[1])
            high            = float(row[2])
            low             = float(row[3])
            close           = float(row[4])
            volume          = int(row[5])
            adjustedClose   = float(row[6])
            
            print("Inserting data into database")
            
            c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
            stockExchangeID = c.fetchone()[0]

            c.execute("SELECT companyID FROM companies WHERE companyCode = %s AND stockExchangeID = %s", [companyCode, stockExchangeID])
            companyID = c.fetchone()[0]
            
            c.execute("SELECT date FROM historicalCompanyData WHERE companyID = %s ORDER BY date DESC;", [companyID])
            lastDate = c.fetchone()[0]
            
            if str(lastDate) == str(date):
                print("Already Up To Date")
            else:
                c.execute("INSERT INTO historicalCompanyData (companyID, stockExchangeID, date, open, high, low, close, volume, adjustedClose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            (companyID, stockExchangeID, date, openPrice, high, low, close, volume, adjustedClose))
                conn.commit()
                
            break

            
            
            
#Returns an array all the indicies and their key values
def getIndexList(stockExchangeCode):
    indicies = []
    with open("../static/csv/stockExchanges/indicies/" + stockExchangeCode + ".csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        for row in readCSV:
            indicies.append(row)
    return indicies

    
    
    
    
def insertIndex(stockExchange, indexName, indexCode):
    c, conn = connection()

    c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchange])
    stockExchangeID = c.fetchone()[0]
    
    doesIndexExist = c.execute("SELECT stockExchangeIndexID from stockExchangeIndicies WHERE stockExchangeID = (%s) AND indexCode = (%s)", [stockExchangeID, indexCode])
    
    if int(doesIndexExist) < 1:
        print("Inserting Into Database")
        c.execute("INSERT INTO stockExchangeIndicies (stockExchangeID, indexName, indexCode) VALUES (%s, %s, %s)", [stockExchangeID, indexName, indexCode])
    else:
        print("Already In The Database")
        
    conn.commit()
    c.close()
    conn.close()
    
    
    
    
#Checks all dates for a company and if the data is missing it adds it
def insertHistoricalIndexData(stockExchangeCode, indexCode):
    c, conn = connection()
    
    with open("../static/csv/stockExchanges/historicalIndexData/" + stockExchangeCode + "/" + indexCode + ".csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        next(readCSV)
        
        for row in list(readCSV):
            latestDate = row[0]
            break

    c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
    stockExchangeID = c.fetchone()[0]

    c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = %s AND stockExchangeID = %s", [indexCode, stockExchangeID])
    stockExchangeIndexID = c.fetchone()[0]
    
    doesLastDateExist = c.execute("SELECT date FROM historicalIndexData WHERE stockExchangeIndexID = %s ORDER BY date DESC;", [stockExchangeIndexID])
    
    if int(doesLastDateExist) > 0:
        lastDate = c.fetchone()[0]
    else:
        lastDate = "No Date"
    
    if str(lastDate) == str(latestDate):
        print("Already Up To Date")
    else:
        print("Updating Data")
        with open("../static/csv/stockExchanges/historicalIndexData/" + stockExchangeCode + "/" + indexCode + ".csv") as csvFile:
            readCSV = csv.reader(csvFile, delimiter=',')
            next(readCSV)
            
            for row in reversed(list(readCSV)):
            
                date            = row[0]
                openPrice       = row[1]
                high            = row[2]
                low             = float(row[3])
                close           = float(row[4])
                adjustedClose   = float(row[6])
                
                doesEntryExist = c.execute("SELECT historicalIndexDataID FROM historicalIndexData WHERE stockExchangeIndexID = (%s) AND stockExchangeID = (%s) AND date = (%s)", [stockExchangeIndexID, stockExchangeID, date])
                
                if doesEntryExist < 1:
                    c.execute("INSERT INTO historicalIndexData (stockExchangeIndexID, stockExchangeID, date, open, high, low, close, adjustedClose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                            (stockExchangeIndexID, stockExchangeID, date, openPrice, high, low, close, adjustedClose))
                            
                    conn.commit()

    c.close()
    conn.close()
    
    
    
    
    
def updateHistoricalIndexData(stockExchangeCode, indexCode):
    c, conn = connection()
    
    with open("../static/csv/stockExchanges/historicalIndexData/" + stockExchangeCode + "/" + indexCode + ".csv") as csvFile:
        readCSV = csv.reader(csvFile, delimiter=',')
        next(readCSV)
        
        for row in list(readCSV):
            date            = row[0]
            openPrice       = row[1]
            high            = row[2]
            low             = float(row[3])
            close           = float(row[4])
            adjustedClose   = float(row[6])
            
            c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
            stockExchangeID = c.fetchone()[0]

            c.execute("SELECT stockExchangeIndexID FROM stockExchangeIndicies WHERE indexCode = %s AND stockExchangeID = %s", [indexCode, stockExchangeID])
            stockExchangeIndexID = c.fetchone()[0]
            
            doesEntryExist = c.execute("SELECT date FROM historicalIndexData WHERE stockExchangeIndexID = %s ORDER BY date DESC;", [stockExchangeIndexID])
            if doesEntryExist > 0:
                lastDate = c.fetchone()[0]
            else:
                lastDate = "No Date"
                        
            if str(lastDate) == str(date):
                print("Already Up To Date")
            else:
                c.execute("INSERT INTO historicalIndexData (stockExchangeIndexID, stockExchangeID, date, open, high, low, close, adjustedClose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                        (stockExchangeIndexID, stockExchangeID, date, openPrice, high, low, close, adjustedClose))
                conn.commit()
                print("Not Up To Date, Inserting Data")
            break
			
			

			
			
#Live Data