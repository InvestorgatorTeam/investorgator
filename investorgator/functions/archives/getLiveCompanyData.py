#Program is started at the beginning of trading day
#Ends at the end of the trading day

from dataMiner import *
from datetime import datetime
import sys
from dataMiner import *
from dataScraper import *    

def modifyTime(current_hour, timeModifier):
    modifiedTime = current_hour + timeModifier
    if modifiedTime >= 24:
        modifiedTime = modifiedTime - 24
    return modifiedTime

if __name__ == "__main__":
    if len(sys.argv) == 2:
        stockExchangeCode = sys.argv[1]
        companies = readCompanyList(stockExchangeCode)
        
        if stockExchangeCode == "AX":
            endHour      = 16
            endMinute    = 30
            timeModifier = 11
        elif stockExchangeCode == "OQ":
            endHour      = 4
            endMinute    = 20
            timeModifier = -5
        elif stockExchangeCode == "N":
            endHour      = 4
            endMinute    = 20
            timeModifier = -5
        
        current_time = datetime.now().time()
        #print(current_time)
        #print(modifyTime(current_time.hour, timeModifier))
        
        c, conn = connection()
        
        c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
        stockExchangeID = c.fetchone()[0]
        
        while modifyTime(current_time.hour, timeModifier) < endHour or current_time.minute <= endMinute:

            current_time = datetime.now().time()
            print(current_time)
            
            for row in companies:
                companyCode = row[1]
                
                #print(companyCode + "." + stockExchangeCode)
                
                try:
                    downloadLiveCompanyDataCSV(stockExchangeCode, companyCode)
                    
                    filePath = "../static/csv/stockExchanges/liveCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv"
                    
                    with open(filePath) as csvFile:
                        readCSV = csv.reader(csvFile, delimiter=',')
                        
                        row = next(readCSV)
                        
                        price     = row[1]
                        date      = row[2]
                        time      = row[3]
                        change    = row[4]
                        openPrice = row[5]
                        highPrice = row[6]
                        lowPrice  = row[7]
                        volume    = row[8]

                        c.execute("SELECT companyID FROM companies WHERE companyCode = %s AND stockExchangeID = %s", [companyCode, stockExchangeID])
                        companyID = c.fetchone()[0]
                        
                        doesEntryExist = c.execute("SELECT date, time FROM liveCompanyData WHERE companyID = %s AND stockExchangeID = %s ORDER BY liveCompanyDataID DESC;", [companyID, stockExchangeID])
                        
                        if int(doesEntryExist) > 0:
                            latestQuote = c.fetchone()
                            lastDate = latestQuote[0]
                            lastTime = latestQuote[1]
                            
                            lastDateTime = lastDate + lastTime
                            dateTime     = date + time

                            if lastDateTime != dateTime:
                                #print("Not up to date, inserting")
                                c.execute("INSERT INTO liveCompanyData (`companyID`, `stockExchangeID`, `price`, `date`, `time`, `change`, `open`, `high`, `low`, `volume`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                            (companyID, stockExchangeID, price, date, time, change, openPrice, highPrice, lowPrice, volume))
                                conn.commit()
                            #else:
                            #    print("Already Up To Date")
                        else:
                            #print("No entry exists, inserting")
                            c.execute("INSERT INTO liveCompanyData (`companyID`, `stockExchangeID`, `price`, `date`, `time`, `change`, `open`, `high`, `low`, `volume`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                                        (companyID, stockExchangeID, price, date, time, change, openPrice, highPrice, lowPrice, volume))
                            conn.commit()
                except:
                    continue
        c.close()
        conn.close()
        
    else:
        #TODO - Add error to log file
        print("Please enter the argument for the desired stock exchange")
#except Exception as e:
#print("No live data: " + str(e))