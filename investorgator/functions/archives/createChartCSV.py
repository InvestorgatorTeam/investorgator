from dataMiner import *
import sys

def convertDate(date):
    year  = date[2:4]
    month = date[5:7]
    day   = date[8:10]
    
    if month == "01":
        month = "Jan"
    elif month == "02":
        month = "Feb"
    elif month == "03":
        month = "Mar"
    elif month == "04":
        month = "Apr"
    elif month == "05":
        month = "May"
    elif month == "06":
        month = "Jun"
    elif month == "07":
        month = "Jul"
    elif month == "08":
        month = "Aug"
    elif month == "09":
        month = "Sep"
    elif month == "10":
        month = "Oct"
    elif month == "11":
        month = "Nov"
    elif month == "12":
        month = "Dec"
    
    return day + "-" + month + "-" + year

if __name__ == "__main__":
    if len(sys.argv) == 2:
        stockExchangeCode = sys.argv[1]
        companies = readCompanyList(stockExchangeCode)
        
        print("Updating Historical Company Data for " + stockExchangeCode + "\n")
        
        for row in companies:
            companyName = row[0]
            companyCode = row[1]
            industry = row[2]
            
            with open("../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv", 'r') as companyDataCSV:
                with open("../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/chartData" + companyCode + ".csv", 'w') as chartDataCSV:
                    companyData = csv.reader(companyDataCSV, delimiter=',')
                    writer      = csv.writer(chartDataCSV, delimiter=',')
                    i = 0
                    for row in companyData:
                        if i > 0:
                            newRow = []
                            
                            newRow.append(convertDate(row[0]))
                            newRow.append("%.2f" % float(row[1]))
                            newRow.append("%.2f" % float(row[2]))
                            newRow.append("%.2f" % float(row[3]))
                            newRow.append("%.2f" % float(row[4]))
                            
                            if row[5][0] == "0":
                                row[5] = 0
                            newRow.append(row[5])
                            
                            writer.writerow(newRow)
                        else:
                            writer.writerow(row)
                            #next(companyData)
                        i = i + 1
                        
        print("Finished making chart data csv")
    else:
        #Add error to log file
        print("Please enter the argument for the desired stock exchange")
    