#Sweeper Functions (CSV Downloads)
import urllib.request
from urllib.request import urlopen

#Yahoo finance sweep to get all historical data for companies for every date
def downloadHistoricalCompanyDataCSV(stockExchangeCode, companyCode):
    if stockExchangeCode == "AX":
        url = 'http://chart.finance.yahoo.com/table.csv?s=' + companyCode + '.' + stockExchangeCode + '&a=00&b=1&c=1000&ignore=.csv'
    elif (stockExchangeCode == "N") or (stockExchangeCode == "OQ"):
        url = 'http://chart.finance.yahoo.com/table.csv?s=' + companyCode + '&a=00&b=1&c=1000&ignore=.csv'
    savePath = "../static/csv/stockExchanges/historicalCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv"
    print(url)
    print(savePath)
    try:
        urllib.request.urlretrieve(url, savePath)
    except Exception as e:
        print("Failed to get historical data for: " + companyCode)
        print(e)




        
#Yahoo finance sweep to get all historical data for indicies for every date
def downloadHistoricalIndexDataCSV(stockExchangeCode, indexCode, scraperCode):
    url = 'http://chart.finance.yahoo.com/table.csv?s=^' + scraperCode + '&a=00&b=1&c=1000&ignore=.csv'
    savePath = "../static/csv/stockExchanges/historicalIndexData/" + stockExchangeCode + "/" + indexCode + ".csv"
    try:
        urllib.request.urlretrieve(url, savePath)
    except Exception as e:
        print("Failed to get historical data for: " + indexCode)
        print(e)

        
        
        
        
#Yahoo finance sweep to get all historical data for companies for every date
def downloadLiveCompanyDataCSV(stockExchangeCode, companyCode):
    if stockExchangeCode == "AX":
        url = 'http://download.finance.yahoo.com/d/quotes.csv?s=' + companyCode + '.' + stockExchangeCode + '&f=sl1d1t1c1ohgv&e=.csv'
    elif (stockExchangeCode == "N") or (stockExchangeCode == "OQ"):
        url = 'http://download.finance.yahoo.com/d/quotes.csv?s=' + companyCode + '&f=sl1d1t1c1ohgv&e=.csv'  
    savePath = "../static/csv/stockExchanges/liveCompanyData/" + stockExchangeCode + "/" + companyCode + ".csv"
    #print(url)
    #print(savePath)
    try:
        urllib.request.urlretrieve(url, savePath)
    except Exception as e:
        print("Failed to get historical data for: " + companyCode)
        #print(e)