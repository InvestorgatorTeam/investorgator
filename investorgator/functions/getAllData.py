from dataMiner import *
from dataScraper import *

def stock_exchange_companies():
    c, conn = connection()
    print("==== Adding Data For Stock Exchange Companies ====\n")
    
    stockExchanges = getStockExchangeList()
    
    for stockExchange in stockExchanges:
        stockExchangeName = stockExchange[0]
        stockExchangeCode = stockExchange[1]
        
        print("== Adding Company Data For " + stockExchangeName + " ==")
        
        #Get array of company info
        print("= Reading Company CSV files =")
        
        companies = readCompanyList(stockExchangeCode)
        
        print("Success\n")
        
        #Make company entries
        print("= Making company entries =")
        
        for company in companies:
            companyName = company[0]
            companyCode = company[1]
            industry    = company[2]
            
            print("Inserting " + companyName + " into the database")
            insertCompany(stockExchangeCode, companyName, companyCode, industry, c, conn)
        
        print("Success\n")
        
        
        #Download historical data for companies
        print("= Downloading data for companies =")
        
        for company in companies:
            companyName = company[0]
            companyCode = company[1]
            industry    = company[2]
            
            print("Downloading data for " + companyName + "(" + companyCode + ")")
            downloadHistoricalCompanyDataCSV(stockExchangeCode, companyCode)
        
        print("Success\n")
        
        
        #Insert all the historical prices for companies from the csv files
        print("= Inserting all the historical prices for companies from the csv files =")
        
        c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeCode = %s", [stockExchangeCode])
        stockExchangeID = c.fetchone()[0]
        
        for company in companies:
            companyName = company[0]
            companyCode = company[1]
            industry    = company[2]
            
            c.execute("SELECT companyID FROM companies WHERE companyCode = %s AND stockExchangeID = %s", [companyCode, stockExchangeID])
            companyID = c.fetchone()[0]
            
            print("Inserting data for " + companyName + " into the database")
            insertHistoricalCompanyData(stockExchangeCode, companyCode, stockExchangeID, companyID, c, conn)
            
        print("Success\n")
        
    print("\n")
    c.close()
    conn.close()
    gc.collect()
    
def stock_exchange_indicies():
    print("=== Adding Data For Stock Exchange Indicies ===\n")

    stockExchanges = getStockExchangeList()
    
    for stockExchange in stockExchanges:
        stockExchangeName = stockExchange[0]
        stockExchangeCode = stockExchange[1]
        
        print("== Adding Index Data For " + stockExchangeName + " ==\n")
        
        #get array of index info
        print("= Getting Array Of Index Info =")
        
        indicies = getIndexList(stockExchangeCode)
        print("Success\n")
        
        #Make index entries
        print("= Making Index Entries =")
        
        for index in indicies:
            indexName = index[0]
            indexCode = index[1]
            
            print("Inserting " + indexName + " Into The Database")
            insertIndex(stockExchangeCode, indexName, indexCode)
            
        print("Success\n")
        
        #Download historical data for indicies
        print("= Downloading data for indicies =")
        
        for index in indicies:
            indexName   = index[0]
            indexCode   = index[1]
            scraperCode = index[2]
            
            print("Downloading data for " + indexName)
            downloadHistoricalIndexDataCSV(stockExchangeCode, indexCode, scraperCode)
            
        print("Success\n")
        
        #Insert data for indicies
        print("= Inserting data for indicies =")
        
        for index in indicies:
            indexName   = index[0]
            indexCode   = index[1]
            
            print("Inserting data for " + indexName + " into the database")
            insertHistoricalIndexData(stockExchangeCode, indexCode)
            
        print("Success\n")
    
if __name__ == "__main__":
    print("==== Filling and Updating Database ====\n")
    
    #Make stock exchanges entries
    
    insertStockExchanges()
    stock_exchange_indicies()
    stock_exchange_companies()
    
    
    print("==== Finished Filling and Updating Database ====")
