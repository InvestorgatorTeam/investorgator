from investorgator import app
from investorgator.functionsStockExchanges import *
from flask import Flask, request, render_template, redirect, url_for, session
from investorgator.connection import connection
from MySQLdb import escape_string as thwart

#Main stock exchanges page
@app.route('/stock_exchanges', methods=['GET'])
@app.route('/stock_exchanges/', methods=['GET'])
def stock_exchanges_dashboard():
    percentageChanges = getIndexPercentageChanges()
    return render_template("stock_exchanges/stock_exchanges.html",
                            percentageChanges = percentageChanges
    )
    
@app.route('/stock_exchanges/all_stock_exchanges', methods=['GET'])
@app.route('/stock_exchanges/all_stock_exchanges/', methods=['GET'])
def stock_exchanges_all_stock_exchanges():
    stockExchanges = getStockExchanges()
    return render_template("stock_exchanges/stock_exchanges_all_stock_exchanges.html",
                            stockExchanges = stockExchanges
    )
    
@app.route('/stock_exchanges/all_companies', methods=['GET'])
@app.route('/stock_exchanges/all_companies/', methods=['GET'])
def stock_exchanges_all_companies():
    companies = getAllCompanies()
    return render_template("stock_exchanges/stock_exchanges_all_companies.html",
                            companies = companies
    )
    


#Page for a stock exchange
@app.route('/stock_exchanges/<stockExchangeAbbrev>', methods=['GET'])
@app.route('/stock_exchanges/<stockExchangeAbbrev>/', methods=['GET'])
def stock_exchange_dashboard(stockExchangeAbbrev=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchangeAbbrev)

    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))
    
    stockExchangeName = getStockExchangeName(stockExchangeID)
    
    #indexName            = getStockExchangeIndexName(stockExchangeIndexID)
    stockExchangeIndexID = getMainStockExchangeIndexID(stockExchangeAbbrev, stockExchangeID)
    currentPrice         = getCurrentStockExchangeIndexPrice(stockExchangeIndexID)
    previousPrice        = getPreviousStockExchangeIndexClosePrice(stockExchangeIndexID) 
    percentageChange     = getPercentageChange(currentPrice, previousPrice)
    changeValue          = getChangeValue(currentPrice, previousPrice)
    color                = getPercentageChangeColor(percentageChange)
    
    #Top 5 advancers
    topAdvancers = getTopAdvancerCompanies(stockExchangeID)
    
    #Top 5 decliners
    topDecliners = getTopDeclinerCompanies(stockExchangeID)
    
    #Top 5 Volume
    topVolume = getTopVolumeCompanies(stockExchangeID)
    
    return render_template("stock_exchanges/stock_exchange_dashboard.html",
                            stockExchangeAbbrev = stockExchangeAbbrev.upper(),
                            stockExchangeName   = stockExchangeName,
                            currentPrice        = currentPrice,
                            percentageChange    = percentageChange,
                            changeValue         = changeValue,
                            color               = color,
                            topVolume           = topVolume,
                            topAdvancers        = topAdvancers,
                            topDecliners        = topDecliners
    )
    
@app.route('/stock_exchanges/<stockExchange>/all_companies', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/all_companies/', methods=['GET'])
def stock_exchange_all_companies(stockExchange=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)

    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    companiesList = getAllStockExchangeCompaniesData(stockExchangeID)
    
    return render_template("stock_exchanges/stock_exchange_all_companies.html",
                            stockExchange = stockExchange.upper(), 
                            companies     = companiesList
    )
    
@app.route('/stock_exchanges/<stockExchange>/indicies', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/indicies/', methods=['GET'])
def stock_exchange_indicies(stockExchange=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)

    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))
    
    indiciesList = getIndiciesList(stockExchangeID)

    return render_template("stock_exchanges/stock_exchange_indicies.html", 
                            stockExchange = stockExchange.upper(), 
                            indicies      = indiciesList
    )
    
@app.route('/stock_exchanges/<stockExchange>/exchange_info', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/exchange_info/', methods=['GET'])
def stock_exchange_exchange_info(stockExchange=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)

    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))
    
    return render_template("stock_exchanges/stock_exchange_exchange_info.html",
                            stockExchange = stockExchange.upper()
    )
    


#Index Pages    
@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>/', methods=['GET'])
def index_dashboard(stockExchange=False, indexCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    doesStockExchangeIndexExist, stockExchangeIndexID = getStockExchangeIndexID(indexCode, stockExchangeID)
    
    if  doesStockExchangeIndexExist == False:
        return redirect(url_for('stock_exchange_dashboard', stockExchange = stockExchange))
        
    indexName        = getStockExchangeIndexName(stockExchangeIndexID)
    currentPrice     = getCurrentStockExchangeIndexPrice(stockExchangeIndexID)
    previousPrice    = getPreviousStockExchangeIndexClosePrice(stockExchangeIndexID) 
    percentageChange = getPercentageChange(currentPrice, previousPrice)
    changeValue      = getChangeValue(currentPrice, previousPrice)
    color            = getPercentageChangeColor(percentageChange)
    
    return render_template("stock_exchanges/index_dashboard.html",
                            stockExchange    = stockExchange.upper(),
                            indexCode        = indexCode.upper(),
                            indexName        = indexName,
                            currentPrice     = currentPrice,
                            percentageChange = percentageChange,
                            changeValue      = changeValue,
                            color            = color
    )

@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>/charts', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>/charts/', methods=['GET'])
def index_charts(stockExchange=False, indexCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    doesStockExchangeIndexExist, stockExchangeIndexID = getStockExchangeIndexID(indexCode, stockExchangeID)
    
    if  doesStockExchangeIndexExist == False:
        return redirect(url_for('stock_exchange_dashboard', stockExchange = stockExchange))
    
    indexName = getStockExchangeIndexName(stockExchangeIndexID)
    chartData = getGoogleIndexChartData(stockExchangeIndexID)
    
    return render_template("stock_exchanges/index_charts.html",
                            stockExchange = stockExchange.upper(),
                            indexCode     = indexCode.upper(),
                            indexName     = indexName,
                            chartData     = chartData
    )
    
@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>/historical_prices', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/indicies/<indexCode>/historical_prices/', methods=['GET'])
def index_historical_prices(stockExchange=False, indexCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    doesStockExchangeIndexExist, stockExchangeIndexID = getStockExchangeIndexID(indexCode, stockExchangeID)
    
    if doesStockExchangeIndexExist == False:
        return redirect(url_for('stock_exchange_dashboard', stockExchange = stockExchange))
    
    indexName = getStockExchangeIndexName(stockExchangeIndexID)
    historicalIndexData = getHistroicalIndexData(stockExchangeIndexID, stockExchangeID)
    
    return render_template("stock_exchanges/index_historical_prices.html",
                            stockExchange       = stockExchange.upper(),
                            indexCode           = indexCode.upper(),
                            indexName           = indexName,
                            historicalIndexData = historicalIndexData
    )

    
    
#Company Pages
@app.route('/stock_exchanges/<stockExchange>/<companyCode>', methods=['GET', 'POST'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/', methods=['GET', 'POST'])
def company_dashboard(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)

    companyName = getCompanyName(companyID)
    industry    = getCompanyIndustry(companyID)

    currentPrice     = getCurrentCompanyPrice(companyID)
    previousPrice    = getPreviousCompanyClosePrice(companyID)
    percentageChange = getPercentageChange(currentPrice, previousPrice)
    changeValue      = getChangeValue(currentPrice, previousPrice)
    color            = getPercentageChangeColor(percentageChange)
    portfolios       = getPortfolios()
    watchlists       = getWatchlists()
    
    return render_template("stock_exchanges/company_dashboard.html", 
                            stockExchange    = stockExchange.upper(), 
                            companyName      = companyName,
                            companyCode      = companyCode.upper(),
                            companyID        = companyID,
                            stockExchangeID  = stockExchangeID,
                            industry         = industry,
                            currentPrice     = currentPrice,
                            percentageChange = percentageChange,
                            changeValue      = changeValue,
                            color            = color,
                            portfolios       = portfolios,
                            watchlists       = watchlists
    )


@app.route('/stock_exchanges/<stockExchange>/<companyCode>/research_and_data', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/research_and_data/', methods=['GET'])
def company_research_and_data(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)

    companyName = getCompanyName(companyID)
        
    return render_template("stock_exchanges/company_research_and_data.html", 
                            stockExchange = stockExchange.upper(),
                            companyCode   = companyCode.upper(),
                            companyName   = companyName
    )

@app.route('/stock_exchanges/<stockExchange>/<companyCode>/charts', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/charts/', methods=['GET'])
def company_charts(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    stockExchangeCode = getStockExchangeCode(stockExchangeID)
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)

    companyName = getCompanyName(companyID)
   
    chartData     = getGoogleCompanyChartData(companyID)
    liveChartData = getLiveGoogleCompanyChartData(companyID)
        
    return render_template("stock_exchanges/company_charts.html", 
                            stockExchange     = stockExchange.upper(),
                            stockExchangeCode = stockExchangeCode.upper(),
                            companyCode       = companyCode.upper(),
                            companyName       = companyName,
                            chartData         = chartData,
                            liveChartData     = liveChartData
    )

@app.route('/stock_exchanges/<stockExchange>/<companyCode>/historical_prices', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/historical_prices/', methods=['GET'])
def company_historical_prices(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    stockExchangeCode = getStockExchangeCode(stockExchangeID)
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)

    companyName = getCompanyName(companyID)

    historicalCompanyData = getHistoricalCompanyPriceData(companyID)

    return render_template("stock_exchanges/company_historical_prices.html", 
                            stockExchange         = stockExchange.upper(), 
                            companyCode           = companyCode.upper(), 
                            historicalCompanyData = historicalCompanyData,
                            companyName           = companyName
    )

@app.route('/stock_exchanges/<stockExchange>/<companyCode>/technical_analysis', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/technical_analysis/', methods=['GET'])
def company_technical_analysis(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    stockExchangeCode = getStockExchangeCode(stockExchangeID)
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)

    companyName = getCompanyName(companyID)
    
    return render_template("stock_exchanges/company_technical_analysis.html", 
                            stockExchange = stockExchange.upper(), 
                            companyCode   = companyCode.upper(),
                            companyName   = companyName
    )
        
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/company_info', methods=['GET'])
@app.route('/stock_exchanges/<stockExchange>/<companyCode>/company_info/', methods=['GET'])
def company_company_info(stockExchange=False, companyCode=False):
    doesStockExchangeExist, stockExchangeID = getStockExchangeID(stockExchange)
    
    if doesStockExchangeExist == False:
        return redirect(url_for('stock_exchanges_dashboard'))

    stockExchangeCode = getStockExchangeCode(stockExchangeID)
    
    doesCompanyExist, companyID = getCompanyID(companyCode, stockExchangeID)
    
    if doesCompanyExist == False:
        return redirect('/stock_exchanges/' + stockExchange)
    
    companyName = getCompanyName(companyID)

    return render_template("stock_exchanges/company_company_info.html", 
                            stockExchange = stockExchange.upper(), 
                            companyCode   = companyCode.upper(),
                            companyName   = companyName
    )

    
    
    
@app.route('/company_search/', methods=['POST'])
def company_search():
    companyCode = request.form['companyCode']
    stockExchangeAbbrev = request.form['stockExchange']

    if stockExchangeAbbrev == '0':
        #Stock Exchanges 

        c, conn = connection()

        result = c.execute("SELECT * FROM companies WHERE companyCode = (%s)", [thwart(companyCode)])

        if int(result) > 0:
            stockExchangeID = c.fetchone()[1]
            c.execute("SELECT stockExchangeAbbrev FROM stockExchanges WHERE stockExchangeID = (%s)", [stockExchangeID])
            stockExchangeAbbrev = c.fetchone()[0]
            return redirect('/stock_exchanges/' + stockExchangeAbbrev.upper() + '/' + companyCode.upper())
        else:
            return redirect('/stock_exchanges')

        c.close()
        conn.close()

        return redirect('/stock_exchanges/ASX/' + companyCode.upper())
    if companyCode:
        #Stock Exchange and Company Page
        c, conn = connection()

        c.execute("SELECT stockExchangeID FROM stockExchanges WHERE stockExchangeAbbrev = (%s)", [thwart(stockExchangeAbbrev)])
        stockExchangeID = c.fetchone()
        result = c.execute("SELECT * FROM companies WHERE companyCode = (%s) AND stockExchangeID = (%s)", [thwart(companyCode), stockExchangeID])

        if int(result) > 0:
            return redirect('/stock_exchanges/' + stockExchangeAbbrev.upper() + '/' + companyCode.upper())
        else:
            return redirect('/stock_exchanges/' + stockExchangeAbbrev)

        c.close()
        conn.close()
    else:
        return redirect('/stock_exchanges')