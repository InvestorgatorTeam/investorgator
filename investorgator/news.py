from investorgator import app
from flask import Flask, request, render_template, redirect, url_for
from investorgator.connection import connection
from investorgator.functionsNews import *

@app.route('/news')
@app.route('/news/')
def news_dashboard():
    newsArticles = getArticles()
    return render_template("news/news_dashboard.html",
                            newsArticles = newsArticles
    )


    
@app.route('/news/article/<newsArticleID>', methods=['GET', 'POST'])
@app.route('/news/article/<newsArticleID>/', methods=['GET', 'POST'])
def article(newsArticleID = False):
    errors = []

    if request.method == "POST":
        if request.form["form"] == "addComment":
            comment = request.form['comment']
            userID  = session['userID']
            errors = addComment(newsArticleID, userID, comment)
            if not errors:
                return redirect(url_for("article", newsArticleID = newsArticleID))

    newsArticle = getArticleData(newsArticleID, "post")
    if newsArticle == None:
        return redirect(url_for("news_dashboard"))
    comments    = getComments(newsArticleID)
    return render_template("news/article.html",
                            newsArticle = newsArticle,
                            comments    = comments,
                            errors      = errors
    )
    
@app.route('/news/author_dashboard', methods=['GET', 'POST'])
@app.route('/news/author_dashboard/', methods=['GET', 'POST'])
def author_dashboard():
    if not session.get('logged_in'):
        return redirect("/news/")
    elif session['permission'] >= 3:
        return redirect("/news/")
        
    if request.method == "POST":
        if request.form["form"] == "deleteArticle":
            deleteArticle(request.form)
            return redirect(url_for("author_dashboard"))
        
    authorArticles = getAuthorArticles()
    otherAuthorArticles = getOtherAuthorArticles()
        
    return render_template("news/author_dashboard.html",
                            authorArticles      = authorArticles,
                            otherAuthorArticles = otherAuthorArticles
    )
 
@app.route('/news/author_dashboard/new_article', methods=['GET', 'POST'])
@app.route('/news/author_dashboard/new_article/', methods=['GET', 'POST'])
def new_article():
    if not session.get('logged_in'):
        return redirect("/news/")
    elif session['permission'] >= 3:
        return redirect("/news/")
    
    errors = []
    
    if request.method == "POST":
        errors = addArticle(request.form, request.files)
        if len(errors) == 0:
            if request.form['postType'] == "post":
                return redirect(url_for("author_dashboard"))
            elif request.form['postType'] == "save":
                return redirect(url_for("drafts"))
            
    return render_template("news/new_article.html",
                            errors = errors
    )

@app.route('/news/author_dashboard/edit_article/<newsArticleID>', methods=['GET', 'POST'])
@app.route('/news/author_dashboard/edit_article/<newsArticleID>/', methods=['GET', 'POST'])
def edit_article(newsArticleID = False):
    if not session.get('logged_in'):
        return redirect(url_for("article", newsArticleID = newsArticleID))
    elif session['permission'] >= 3:
        return redirect(url_for("article", newsArticleID = newsArticleID))
    
    errors = []
    
    if request.method == "POST":
        errors = editArticle(request.form, request.files, newsArticleID)
        if len(errors) == 0:
            return redirect(url_for("author_dashboard"))
    
    newsArticle = getArticleData(newsArticleID, "edit")
    if newsArticle == None:
        return redirect(url_for("author_dashboard"))
    
    return render_template("news/edit_article.html",
                            errors = errors,
                            newsArticle = newsArticle
    )
 
@app.route('/news/author_dashboard/drafts', methods=['GET', 'POST'])
@app.route('/news/author_dashboard/drafts/', methods=['GET', 'POST'])
def drafts():
    if not session.get('logged_in'):
        return redirect("/news/")
    elif session['permission'] >= 3:
        return redirect("/news/")
        
    if request.method == "POST":
        if request.form['form'] == "deleteDraft":
            deleteDraft(request.form)
            return redirect("/news/author_dashboard/drafts")
        elif request.form['form'] == "postDraft":
            postDraft(request.form)
            return redirect("/news/author_dashboard/drafts")
        
    drafts = getDrafts()
        
    return render_template("news/drafts.html",
                            drafts = drafts
    )
    
@app.route('/news/author_dashboard/deleted_articles', methods=['GET', 'POST'])
@app.route('/news/author_dashboard/deleted_articles/', methods=['GET', 'POST'])
def deleted_articles():
    if not session.get('logged_in'):
        return redirect("/news/")
    elif session['permission'] >= 3:
        return redirect("/news/")
        
    if request.method == "POST":
        if request.form['form'] == "undoDelete":
            undoDelete(request.form)
            return redirect(url_for("deleted_articles"))
        elif request.form['form'] == "sendToDrafts":
            sendToDrafts(request.form)
            return redirect(url_for("deleted_articles"))
        elif request.form['form'] == "permanentlyDelete":
            permanentlyDelete(request.form)
            return redirect(url_for("deleted_articles"))
        else:
            return redirect(url_for("deleted_articles"))
        
    deletedArticles = getDeletedArticles()
        
    return render_template("news/deleted_articles.html",
                            deletedArticles = deletedArticles
    )